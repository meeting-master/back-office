## Node image
FROM node:14 as build

## Env variables
ENV TINI_VERSION v0.19.0
ENV NODE_ENV production
ENV NPM_CONFIG_LOGLEVEL info
ARG URL

# Add Tini
# https://github.com/krallin/tini
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini
ENTRYPOINT ["/tini", "--"]

# Create app directory
WORKDIR /app

# Bundle app source
COPY . .
#COPY --chown=node:node . .
# Install dependancie
ENV REACT_APP_API_HOST=$URL
RUN npm install --production

RUN npm run build

# production environment
FROM nginx:stable-alpine
COPY --from=build /app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
