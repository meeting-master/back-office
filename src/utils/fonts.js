const FONTS = {
    Regular: 'Regular',
    Italic: 'Italic',
    Bold: 'Bold',
    SemiBold: 'SemiBold',
    ExtraBold: 'ExtraBold'
};
export default FONTS
