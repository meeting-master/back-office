import React, { useEffect, useState } from 'react';
import { useSelector } from "react-redux";
import { Row, Col, Button, Spinner } from "react-bootstrap";
import TextField from '@material-ui/core/TextField';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import 'date-fns';

//hooks
import useRematchDispatch from "../../hooks/useRematchDispatch";
import { useTranslation } from "react-i18next";

//components
import OfficesList from "../../components/officesList";
import Page from "../../components/Page";
import PageTitle from "../../components/PageTitle";
import {FilePlus} from "react-feather";

const TsGenerationScreen = () => {
    const {t} = useTranslation();
    const {GenerateTimeSlots, GetOffices} = useRematchDispatch(dispatch => ({
        GenerateTimeSlots: dispatch.calendar.GenerateTimeSlots,
        GetOffices: dispatch.customer.GetOffices,
    }));

    const ongoing = useSelector(state => state.calendar.ongoing);
    const offices = useSelector(state => state.customer.offices);
    const customerId = useSelector(state => state.preference.customerId);

    const [serviceId, setServiceId] = useState(null);
    const [place, setPlace] = useState(1);
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date().setMonth(new Date().getMonth() + 1));

    useEffect(() => {
        GetOffices(customerId);
    }, []);

    useEffect(() => {
        if (!ongoing) {
            setServiceId(null);
            init()
        }
    }, [ongoing]);

    const init = () => {
        setPlace(1);
        initTime();
    };

    const initTime = () => {
        setStartDate(new Date());
        setEndDate(new Date().setMonth(new Date().getMonth() + 1));
    };

    const officeServiceSelected = (officeId, serviceId) => {
        setServiceId(serviceId);
        init()
    };

    const handleSubmitClick = () => {
        GenerateTimeSlots({itemId:serviceId, maximum: place, startDate: new Date(startDate), endDate: new Date(endDate)});
    };

    const onPlaceChange = (value) => {
        setPlace(parseInt(value));
    };

    const disabledClick = () => {
        return place <= 0 || serviceId === null;
    };

    const datePicker = () => {
        return (
            <>
                <Row>
                    <Col>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                disableToolbar
                                variant="inline"
                                format={t('date_format')}
                                margin="normal"
                                id="startDate"
                                label={t('start_date')}
                                value={startDate}
                                onChange={(v) => setStartDate(v)}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                                className="col-xs-12 col-md-6 pr-2 mb-2"
                            />
                            <KeyboardDatePicker
                                disableToolbar
                                variant="inline"
                                format={t('date_format')}
                                margin="normal"
                                id="endDate"
                                label={t('end_date')}
                                value={endDate}
                                onChange={(v) => setEndDate(v)}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                                className="col-xs-12 col-md-6"
                            />
                        </MuiPickersUtilsProvider>
                    </Col>
                </Row>
            </>
        )
    };
    return (
        <Page>
            <div className="container" >
                <Row>
                    <Col className="mb-3">
                        <PageTitle Icon={FilePlus} title={t(`labels.menu_calendar_generate`)}/>
                    </Col>
                </Row>
                <Row>
                    <Col xs="12" xl={9}><OfficesList offices={offices} onSelected={officeServiceSelected}/></Col>
                    <Col xs="12" xl={3}>{datePicker()}</Col>
                </Row>
                <span className="space"/>
                <Row>
                    <Col className="d-flex justify-content-center">
                        <TextField id="place" label={(t('place'))} type="number" value={place}
                                   style={{width: 200, marginRight: 20}}
                                   onChange={(event => onPlaceChange(event.target.value))}/>
                        <Button variant="outline-dark" onClick={() => {
                            handleSubmitClick()
                        }} style={{width: '40%'}} disabled={disabledClick()}>
                            {ongoing ?
                                <Spinner as="span" animation="grow" size="sm" role="status"
                                         aria-hidden="true"/> : null} {t('generate_time_slot')}</Button>
                    </Col>
                </Row>
            </div>
            <span className="space"/>
            <span className="space"/>
            <span className="space"/>
        </Page>
    )
};

export default TsGenerationScreen
