import React, {useEffect, useState} from 'react';
import {useSelector} from "react-redux";
import {Accordion, Col, Row} from "react-bootstrap";
import TextField from '@material-ui/core/TextField';
import DayPicker from "react-day-picker";
import 'react-day-picker/lib/style.css';
import {MenuItem, useMediaQuery, useTheme} from "@material-ui/core";
import {AddCircle} from "@material-ui/icons";
import {ChevronDown, ChevronUp} from "react-feather";

//services
import useRematchDispatch from "../../hooks/useRematchDispatch";
import {useTranslation} from "react-i18next";
import {AppStyles} from "../../css/styles";
import Colors from "../../utils/Colors";
import FONTS from "../../utils/fonts";

//components
import Page from "../../components/Page";
import AppointmentContent from "./appointmentContent";

const AppointmentScreen = () => {
    const {t} = useTranslation();
    const {GetDayCalendar, GetOffices, GetServices, SetRuntimeError} = useRematchDispatch(dispatch => ({
        GetDayCalendar: dispatch.calendar.GetDayCalendar,
        GetOffices: dispatch.customer.GetOffices,
        GetServices: dispatch.customer.GetServices,
        SetRuntimeError: dispatch.main.SetRuntimeError,
    }));

    const offices = useSelector(state => state.customer.offices);
    const services = useSelector(state => state.customer.services);
    const customerId = useSelector(state => state.preference.customerId);
    const dayCalendar = useSelector(state => state.calendar.dayCalendar);
    const reload = useSelector(state => state.calendar.reload);

    const [serviceId, setServiceId] = useState(null);
    const [officeId, setOfficeId] = useState('');
    const [officeServices, setOfficeServices] = useState([]);
    const [selectedDay, setSelectedDay] = useState(new Date());
    const [selectedTs, setSelectedTs] = useState();
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.up('md'));

    useEffect(() => {
        if (customerId !== null) {
            GetServices(customerId);
            GetOffices(customerId);
        }
        // eslint-disable-next-line
    }, [customerId]);

    useEffect(() => {
        if (reload && officeId && serviceId && selectedDay) {
            GetDayCalendar({officeId, serviceId, day: selectedDay})
        }
// eslint-disable-next-line
    }, [reload]);

    useEffect(() => {
        let servs = [];
        const office = offices.find(x => x.id === officeId);
        if (office) {
            const os = office.serviceIds;
            services.forEach((s) => {
                if (os.includes(s.id)) {
                    servs.push(s);
                }
            })
        }
        setOfficeServices(servs);
        // eslint-disable-next-line
    }, [officeId]);

    const onMonthChange = (startDay) => {
    };

    const onDayClick = (day, modifiers, e) => {
        setSelectedDay(day);
        if (officeId && serviceId) {
            GetDayCalendar({officeId, serviceId, day})
        } else {
            SetRuntimeError('no_office_service')
        }

    };

    const handleChevronClick = (id) => {
        setSelectedTs(id === selectedTs ? '' : id)
    };

    const renderTs = (ts) => {
        const Icon = selectedTs === ts.id ? ChevronUp : ChevronDown;
        let st = {
            color: ts.available > 0 ? Colors.black : Colors.white,
            height: 40,
            fontFamily: FONTS.Regular,
            fontSize: 12,
            backgroundColor: ts.available > 0 ? Colors.grey : Colors.meanBlue
        };


        return (
            <Row className="mb-3" key={ts.id}>
                <Col>
                    <Row style={st} className="align-items-center justify-content-center align-self-center">
                        <Col xs={3}>
                            {`${ts.startTime} - ${ts.endTime}`}
                        </Col>
                        <Col xs={3}>
                            {`${ts.consumed} ${t('labels.appointment')} / ${ts.available + ts.consumed}`}
                        </Col>
                        <Col xs={6} className="d-flex flex-row-reverse">
                            <Accordion.Toggle eventKey={ts.id}>
                                <Icon style={styles.chevron} onClick={() => handleChevronClick(ts.id)} size={15}/>
                            </Accordion.Toggle>
                        </Col>
                    </Row>
                    <Accordion.Collapse eventKey={ts.id}>
                        {selectedTs === ts.id ? (
                            <AppointmentContent timeSlot={ts}/>
                        ) : <div/>}
                    </Accordion.Collapse>
                </Col>
            </Row>
        )
    };

    return (
        <Page title={'EasyMeeting ' + t(`menu_appointment`)}>
            <main className="container">
                <Row className="greyBar">
                    <Col className="d-flex align-items-center">
                        <AddCircle style={{color: Colors.black}}/> <span style={AppStyles.greyBarText}
                                                                         className="ml-2"> {t('labels.view_appointment')}</span>
                    </Col>
                </Row>
                <Row className="text-center mb-2">
                    <Col xs={12} md={1}/>
                    <Col xs={12} md={3}>
                        <TextField id="officeId" select label={t('office')} value={officeId} required
                                   size="medium" className="w-100" InputProps={AppStyles.input}
                                   InputLabelProps={AppStyles.input}
                                   error={false}
                                   onChange={(event) => setOfficeId(event.target.value)}>

                            {offices.filter(x => x.isArchived === false).map((x) => (
                                <MenuItem key={x.id} value={x.id} style={{fontSize: 10}}>
                                    {x.name}
                                </MenuItem>
                            ))}
                        </TextField>
                    </Col>

                    <Col xs={12} md={3}>
                        <TextField id="serviceId" select label={t('service')} value={serviceId} required
                                   size="medium" className="w-100" InputProps={AppStyles.input}
                                   InputLabelProps={AppStyles.input}
                                   error={false}
                                   onChange={(event) => setServiceId(event.target.value)}>

                            {officeServices.map((x) => (
                                <MenuItem key={x.id} value={x.id} style={{fontSize: 10}}>
                                    {x.name}
                                </MenuItem>
                            ))}
                        </TextField>
                    </Col>
                    <Col xs={12} md={4}>
                        <DayPicker onMonthChange={onMonthChange} onDayClick={onDayClick} selectedDays={selectedDay}
                                   style={AppStyles.input}
                                   month={selectedDay} disabledDays={{before: new Date()}} className="w-100"/>
                    </Col>
                    <Col xs={12} md={1}/>
                </Row>
                {matches &&
                <Row style={styles.listH}>
                    <Col xs={3} xl={1}>{t('labels.schedule')}</Col>
                    <Col xs={3} xl={2}>{t('first_name')}</Col>
                    <Col xs={3} xl={2}>{t('last_name')}</Col>
                    <Col xs={3} xl={1}>{t('phone_number')}</Col>
                    <Col xs={3} xl={2}>{t('email')}</Col>
                    <Col xs={4} xl={1}>{t('place')}</Col>
                    <Col xs={4} xl={1}>{t('creation_date')}</Col>
                    <Col xs={4} xl={1}>{t('used_date')}</Col>
                    <Col xs={12} xl={1}/>
                </Row>}
                <Accordion>
                    {dayCalendar.map(ts => renderTs(ts))}
                </Accordion>
            </main>
        </Page>

    )
};

const styles = {
    listH: {
        backgroundColor: '#373737',
        height: 40,
        color: Colors.white,
        paddingTop: 10,
        fontFamily: FONTS.Regular,
        fontSize: 12
    },
    accordionH: {
        color: Colors.white,
        height: 40,
        fontFamily: FONTS.Regular,
        fontSize: 12
    },
    chevron: {
        color: Colors.white,
    }
};
export default AppointmentScreen
