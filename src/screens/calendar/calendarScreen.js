import React, {useEffect, useState} from 'react';
import {Accordion, Col, Row} from "react-bootstrap";
import 'react-day-picker/lib/style.css';
import {ChevronDown, ChevronUp} from 'react-feather';
import {AddCircle, Visibility} from "@material-ui/icons";
import {useSelector} from "react-redux";
import {MenuItem, TextField} from "@material-ui/core";
import Select from "react-select";
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import format from "date-fns/format";

//services
import useRematchDispatch from "../../hooks/useRematchDispatch";
import {useTranslation} from "react-i18next";
import Colors from "../../utils/Colors";
import {AppStyles} from '../../css/styles';

//components
import Page from "../../components/Page";
import {CancelButton, ValidationButton} from "../../components/Buttons";

const CalendarScreen = () => {
    const {t} = useTranslation();
    const {
        LoadLocations,
        GetOffices,
        GetServices,
        GenerateTimeSlots,
        GetCalendarHistory
    } = useRematchDispatch(dispatch => ({
        LoadLocations: dispatch.location.LoadLocations,
        GetOffices: dispatch.customer.GetOffices,
        GetServices: dispatch.customer.GetServices,
        GenerateTimeSlots: dispatch.calendar.GenerateTimeSlots,
        GetCalendarHistory: dispatch.calendar.GetCalendarHistory,
    }));
    const offices = useSelector(state => state.customer.offices);
    const services = useSelector(state => state.customer.services);
    const locations = useSelector(state => state.location.locations);
    const customerId = useSelector(state => state.preference.customerId);
    //const ongoing = useSelector(state => state.calendar.ongoing);
    const calendarHistory = useSelector(state => state.calendar.calendarHistory);

    const [officeId, setOfficeId] = useState('');
    const [officeServices, setOfficeServices] = useState([]);
    const [serviceIds, setServiceIds] = useState([]);
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date().setMonth(new Date().getMonth() + 1));
    const [selectedOfficeId, setSelectedOfficeId] = useState(null);
    const [selectedServiceId, setSelectedServiceId] = useState(null);

    useEffect(() => {
        if (customerId !== null) {
            GetServices(customerId);
            GetOffices(customerId);
        }
        if (locations.length === 0) {
            LoadLocations();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        let servs = [];
        const office = offices.find(x => x.id === officeId);
        if (office) {
            const os = office.serviceIds;
            services.filter(x => x.isArchived !== true).forEach((s) => {
                if (os.includes(s.id)) {
                    servs.push({value: s.id, label: s.name});
                }
            })
        }
        setOfficeServices(servs);
        // eslint-disable-next-line
    }, [officeId]);

    const onServiceSelected = (ids) => {
        const v = ids.map(x => x.value);
        setServiceIds(v);
    };

    const handleSubmitClick = () => {
        GenerateTimeSlots({officeId, serviceIds, startDate: new Date(startDate), endDate: new Date(endDate)});
    };
    const enableSave = () => {
        return serviceIds.length > 0 && officeId;
    };

    const handleResetClick = () => {
        setOfficeServices([]);
        setServiceIds([]);
        setOfficeId('');

    };

    const handleChangeClick = (officeId, serviceId) => {
        if (officeId === selectedOfficeId && serviceId === selectedServiceId) {
            setSelectedOfficeId('');
            setSelectedServiceId('');
        } else {
            setSelectedOfficeId(officeId);
            setSelectedServiceId(serviceId);
            GetCalendarHistory({officeId, serviceId})
        }

    };

    const renderX = () => {
        return (
            <>
                <Row className="text-center">
                    <Col xs={12} md={1}/>
                    <Col xs={12} md={2}>
                        <TextField id="officeId" select label={t('office')} value={officeId} required
                                   size="medium" className="w-100" InputProps={AppStyles.input}
                                   InputLabelProps={AppStyles.input}
                                   error={false}
                                   onChange={(event) => setOfficeId(event.target.value)}>

                            {offices.filter(x => x.isArchived === false).map((x) => (
                                <MenuItem key={x.id} value={x.id} style={{fontSize: 10}}>
                                    {x.name}
                                </MenuItem>
                            ))}
                        </TextField>
                    </Col>

                    <Col xs={12} md={4}>
                        <Select options={officeServices} isClearable isMulti closeMenuOnSelect={false}
                                styles={AppStyles.Select} placeholder={'Services*'}
                                onChange={onServiceSelected}/>
                    </Col>
                    <Col xs={12} md={2}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker disableToolbar variant="inline" format={t('date_format')}
                                                id="startDate" label={t('start_date')} value={startDate}
                                                onChange={(v) => setStartDate(v)}
                                                KeyboardButtonProps={{'aria-label': 'change date',}}
                                                InputProps={AppStyles.input} InputLabelProps={AppStyles.input}
                            />
                        </MuiPickersUtilsProvider>
                    </Col>
                    <Col xs={12} md={2}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker disableToolbar variant="inline" format={t('date_format')}
                                                id="endDate" label={t('end_date')} value={endDate}
                                                onChange={(v) => setEndDate(v)}
                                                KeyboardButtonProps={{'aria-label': 'change date',}}
                                                InputProps={AppStyles.input} InputLabelProps={AppStyles.input}
                            />
                        </MuiPickersUtilsProvider>
                    </Col>
                    <Col xs={12} md={1}/>
                </Row>
                <Row className="mt-4 mb-4">
                    <Col xs={12} md={1}/>
                    <Col xs={8} md={3}>
                        <ValidationButton onClick={handleSubmitClick} enabled={enableSave()}
                                          label={t('button.calendar_generate')}/>
                    </Col>
                    <Col xs={4} md={3}>
                        <CancelButton onClick={handleResetClick}/>
                    </Col>
                    <Col xs={12} md={4}/>
                </Row>
            </>
        )
    };

    const getDisplayValue = (name, districtId) => {
        let value = name;
        const loc = locations.find(loc => loc.id === districtId);
        if (loc) {
            value = name + " - " + loc.location
        }
        return value;
    };

    const renderHistory = () => {
        return (
            <>
                {calendarHistory.map((history, i) => {
                    return (
                        <Row className="editZone" key={i}>
                            <Col xs={12} md={1}/>
                            <Col xs={4} md={3}>
                                <span
                                    style={AppStyles.greyBarText}>{t('creation_date') + ": " + format(Date.parse(history.createdAt), t('date_format'))}</span>
                            </Col>
                            <Col xs={4} md={3}>
                                <span
                                    style={AppStyles.greyBarText}>{t('start_date') + ": " + format(Date.parse(history.startDate), t('date_format'))}</span>
                            </Col>
                            <Col xs={4} md={3}>
                                <span
                                    style={AppStyles.greyBarText}>{t('end_date') + ": " + format(Date.parse(history.endDate), t('date_format'))}</span>
                            </Col>
                            <Col xs={12} md={1}/>
                        </Row>)
                })}
            </>
        );
    };

    const renderOfficeService = (officeId, officeName, serviceId) => {
        const iconStyle = selectedOfficeId === officeId && selectedServiceId === serviceId ? AppStyles.ActiveIcon : AppStyles.normalIcon;
        const Icon = selectedOfficeId === officeId && selectedServiceId === serviceId ? ChevronUp : ChevronDown;
        const key = officeId + "" + serviceId;
        const s = services.find(x => x.id === serviceId);

        return (
            <Row className="shadow mb-3" key={key}>
                <Col>
                    <Row className="align-items-center justify-content-center align-self-center"
                         style={AppStyles.accordionHeader}>
                        <Col xs={12} md={1}/>
                        <Col xs={4} md={4}>
                            {officeName}
                        </Col>
                        <Col xs={4} md={4}>
                            {s?.name}
                        </Col>

                        <Col xs={2} md={2} className="d-flex flex-row-reverse">
                            <Accordion.Toggle eventKey={key}>
                                <div onClick={() => handleChangeClick(officeId, serviceId)}
                                     style={AppStyles.modifyButton}>
                                    <Icon style={iconStyle} size={15}/>
                                </div>
                            </Accordion.Toggle>
                        </Col>
                    </Row>
                    <Accordion.Collapse eventKey={key}>
                        {selectedOfficeId === officeId && selectedServiceId === serviceId ? (
                            renderHistory()
                        ) : <div/>}
                    </Accordion.Collapse>
                </Col>
            </Row>

        )
    };

    const renderOffice = (office) => {
        const officeName = getDisplayValue(office.name, office.districtId);
        return (
            <>
                {office.serviceIds.map(id => {
                    return renderOfficeService(office.id, officeName, id)
                })}
            </>
        )
    };

    return (
        <Page title={'EasyMeeting ' + t(`menu_calendar`)}>
            <main className="container">
                <Row className="greyBar">
                    <Col className="d-flex align-items-center">
                        <AddCircle style={{color: Colors.black}}/> <span style={AppStyles.greyBarText}
                                                                         className="ml-2"> {t('labels.create_calendar')}</span>
                    </Col>
                </Row>
                {renderX()}
                <Row className="greyBar">
                    <Col className="d-flex align-items-center">
                        <Visibility style={{color: Colors.black}}/> <span style={AppStyles.greyBarText}
                                                                          className="ml-2"> {t('labels.view_calendar')}</span>
                    </Col>
                </Row>
                <Accordion>
                    {offices.map(office => renderOffice(office))}
                </Accordion>
            </main>
        </Page>
    )
};

export default CalendarScreen
