import React, {useEffect, useState} from 'react';
import {useSelector} from "react-redux";
import {Accordion, Col, Row} from "react-bootstrap";
import {useTranslation} from "react-i18next";
import {AddCircle, Edit, Visibility} from "@material-ui/icons";

//services
import useRematchDispatch from "../../hooks/useRematchDispatch";
import {AppStyles} from "../../css/styles";
import Colors from "../../utils/Colors";

//components
import Page from "../../components/Page";
import ScheduleContent from "./scheduleContent";
import ScheduleView from "./scheduleView";


const ScheduleScreen = () => {
    const {t} = useTranslation();
    const {GetOffices, GetServices, LoadLocations} = useRematchDispatch(dispatch => ({
        GetOffices: dispatch.customer.GetOffices,
        GetServices: dispatch.customer.GetServices,
        LoadLocations: dispatch.location.LoadLocations,
    }));

    const offices = useSelector(state => state.customer.offices);
    const services = useSelector(state => state.customer.services);
    const customerId = useSelector(state => state.preference.customerId);
    const locations = useSelector(state => state.location.locations);

    const [selectedOfficeId, setSelectedOfficeId] = useState(null);
    const [selectedServiceId, setSelectedServiceId] = useState(null);


    useEffect(() => {
        GetOffices(customerId);
        GetServices(customerId);
        if (locations.length === 0) {
            LoadLocations();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [customerId]);

    const getDisplayValue = (name, districtId) => {
        let value = name;
        const loc = locations.find(loc => loc.id === districtId);
        if (loc) {
            value = name + " - " + loc.location
        }
        return value;
    };

    const getServiceName = id => {
        const service = services.find(x => x.id === id);
        return service ? service.name : "";
    };

    const handleChangeClick = (officeId, serviceId) => {
        if (officeId === selectedOfficeId && serviceId === selectedServiceId) {
            setSelectedOfficeId('');
            setSelectedServiceId('');
        } else {
            setSelectedOfficeId(officeId);
            setSelectedServiceId(serviceId);
        }

    };


    const renderOfficeService = (officeId, officeName, serviceId) => {
        const iconStyle = selectedOfficeId === officeId && selectedServiceId === serviceId ? AppStyles.ActiveIcon : AppStyles.normalIcon;
        const textStyle = selectedOfficeId === officeId && selectedServiceId === serviceId ? AppStyles.modifyButtonText : AppStyles.greyBarText;
        const key = officeId + "" + serviceId;

        return (
            <Row className="shadow mb-3" key={key}>
                <Col>
                    <Row className="align-items-center justify-content-center align-self-center"
                         style={AppStyles.accordionHeader}>
                        <Col xs={12} md={1}/>
                        <Col xs={4} md={4}>
                            {officeName}
                        </Col>
                        <Col xs={4} md={4}>
                            {getServiceName(serviceId)}
                        </Col>

                        <Col xs={2} md={2} className="d-flex flex-row-reverse">
                            <Accordion.Toggle eventKey={key}>
                                <div onClick={() => handleChangeClick(officeId, serviceId)}
                                     style={AppStyles.modifyButton}>
                                    <Edit style={iconStyle}/>
                                    <span style={textStyle} className="ml-2"> {t('modify')}</span>
                                </div>
                            </Accordion.Toggle>
                        </Col>
                    </Row>
                    <Accordion.Collapse eventKey={key}>
                        {selectedOfficeId === officeId && selectedServiceId === serviceId ? (
                            <ScheduleView officeId={officeId} serviceId={serviceId}/>
                        ) : <div/>}
                    </Accordion.Collapse>
                </Col>
            </Row>

        )
    };

    const renderOffice = (office) => {
        const officeName = getDisplayValue(office.name, office.districtId);
        return (
            <>
                {office.serviceIds.map(id => {
                    return renderOfficeService(office.id, officeName, id)
                })}
            </>
        )
    };
    return (
        <Page title={'EasyMeeting ' + t(`menu_schedule`)}>
            <main className="container">
                <Row className="greyBar">
                    <Col className="d-flex align-items-center">
                        <AddCircle style={{color: Colors.black}}/> <span style={AppStyles.greyBarText}
                                                                         className="ml-2"> {t('labels.create_schedule')}</span>
                    </Col>
                </Row>
                <ScheduleContent customerId={customerId} offices={offices} services={services}/>
                <Row className="greyBar">
                    <Col className="d-flex align-items-center">
                        <Visibility style={{color: Colors.black}}/> <span style={AppStyles.greyBarText}
                                                                          className="ml-2"> {t('labels.view_schedules')}</span>
                    </Col>
                </Row>
                <Accordion>
                    {offices.map(office => renderOffice(office))}
                </Accordion>
            </main>
        </Page>
    )
};

export default ScheduleScreen
