import React, {useEffect, useState} from 'react';
import {useSelector} from "react-redux";
import {Col, Row} from "react-bootstrap";
import {useTranslation} from "react-i18next";
import {TextField, useMediaQuery, useTheme} from '@material-ui/core';
import PropTypes from "prop-types";
import format from "date-fns/format";

//services
import useRematchDispatch from "../../hooks/useRematchDispatch";
import Colors from "../../utils/Colors";
import FONTS from "../../utils/fonts";

//components
import {CancelButton, ValidationButton} from "../../components/Buttons";


const AppointmentContent = ({timeSlot}) => {
    const {t} = useTranslation();
    const {LoadTimeSlotAppointments, CreateAppointment, CancelAppointment, ConsumeAppointment} = useRematchDispatch(dispatch => ({
        LoadTimeSlotAppointments: dispatch.calendar.GetTimeSlotAppointments,
        CreateAppointment: dispatch.calendar.CreateAppointment,
        CancelAppointment: dispatch.calendar.CancelAppointment,
        ConsumeAppointment: dispatch.calendar.ConsumeAppointment,
    }));

    const ongoing = useSelector(state => state.calendar.ongoing);
    const timeSlotAppointments = useSelector(state => state.calendar.timeSlotAppointments);
    const currentCustomer = useSelector(state => state.customer.currentCustomer);

    const [place, setPlace] = useState(1);
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");
    const [email, setEmail] = useState("");
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.up('md'));

    const [errors, setErrors] = useState({
        firstName: false,
        phoneNumber: false,
        lastName: false,
        place: false,
    });

    useEffect(() => {
        LoadTimeSlotAppointments([timeSlot.id])
    }, [timeSlot, LoadTimeSlotAppointments]);

    useEffect(() => {
        if (!ongoing) {
            setFirstName("");
            setLastName("");
            setPhoneNumber("");
            setEmail("");
            setPlace(1);
        }
    }, [ongoing]);


    const handleDeleteClick = (app) => {
        CancelAppointment(`${app.id}|${app.owner !== undefined ? app.owner : ""}`);
    };

    const handleConsumeClick = (app) => {
        ConsumeAppointment(app.id);
    };

    const handleSubmitClick = () => {
        if (!firstName) {
            setErrors(errs => ({...errs, "firstName": true}));
            return;
        } else {
            setErrors(errs => ({...errs, "firstName": false}));
        }

        if (!lastName) {
            setErrors(errs => ({...errs, "lastName": true}));
            return;
        } else {
            setErrors(errs => ({...errs, "lastName": false}));
        }

        if (!phoneNumber) {
            setErrors(errs => ({...errs, "phoneNumber": true}));
            return;
        } else {
            setErrors(errs => ({...errs, "phoneNumber": false}));
        }

        if (place <= 0 || place > timeSlot.available) {
            setErrors(errs => ({...errs, "place": true}));
            return;
        } else {
            setErrors(errs => ({...errs, "place": false}));
        }

        const detail = JSON.stringify({firstName, lastName, phoneNumber, email});
        CreateAppointment({timeSlotId: timeSlot.id, placeNumber: place, detail});
    };


    const frameContainer = () => {
        return (
            <Row className="text-center mb-2 mt-1">
                <Col xs={3} xl={2}> <TextField id="first_name" label={(t('first_name'))} type="text" value={firstName}
                                               size="medium" className="w-100"
                                               InputLabelProps={styles.input} inputProps={styles.input} error={errors.firstName}
                                               onChange={(event => setFirstName(event.target.value))}/>
                </Col>
                <Col xs={3} xl={2}> <TextField id="last_name" label={(t('last_name'))} type="text" value={lastName}
                                               size="medium" className="w-100"
                                               InputLabelProps={styles.input} inputProps={styles.input} error={errors.lastName}
                                               onChange={(event => setLastName(event.target.value))}/>
                </Col>
                <Col xs={3} xl={2}><TextField id="phone_numb" label={(t('phone_number'))} type="text"
                                              value={phoneNumber}
                                              size="medium" className="w-100"
                                              InputLabelProps={styles.input} inputProps={styles.input} error={errors.phoneNumber}
                                              onChange={(event => setPhoneNumber(event.target.value))}/>
                </Col>
                <Col xs={4} xl={3}><TextField id="email" label={(t('email'))} type="text" value={email} size="medium"
                                              className="w-100"
                                              InputLabelProps={styles.input} inputProps={styles.input}
                                              onChange={(event => setEmail(event.target.value))}/></Col>
                <Col xs={3} xl={1}> <TextField id="place" type="number" label={t('place')} error={errors.place}
                                               onChange={(event) => setPlace(parseInt(event.target.value))}
                                               value={place} size="medium" className="w-100" InputProps={styles.input}
                                               InputLabelProps={styles.input}/>
                </Col>
                <Col xs={12} xl={2} className="mt-1 mt-xl-0">
                    <ValidationButton onClick={handleSubmitClick} enabled={currentCustomer?.value > 0} label={t('button.reserve')}/>
                </Col>
            </Row>
        )
    };

    const renderList = () => {
        if (!matches) {
            return renderListXs();
        }
        return (
            <>
                {timeSlotAppointments.map((x, i) => {
                        const user = x.details !== "" ? JSON.parse(x.details) : {};
                        const bg = i % 2 === 0 ? styles.bgOne : styles.bgTwo;
                        return (
                            <Row className="mb-2 mt-1 align-items-center justify-content-center align-self-center" key={i}
                                 style={bg}>
                                <Col xs={12} xl={1}/>
                                <Col xs={6} xl={2}>{user.firstName}</Col>
                                <Col xs={6} xl={2}>{user.lastName}</Col>
                                <Col xs={6} xl={1}>{user.phoneNumber}</Col>
                                <Col xs={6} xl={2}>{user.email}</Col>
                                <Col xs={4} xl={1}>{x.placeNumber}</Col>
                                <Col xs={4} xl={1}>{format(Date.parse(x.createdAt), t('date_format'))}</Col>
                                <Col xs={4} xl={1}>{x.consumedAt === null ? "" : format(Date.parse(x.consumedAt), t('date_format'))}</Col>
                                <Col xs={12} xl={1} className="mt-1 mt-xl-0">
                                    {timeSlot?.isPassed === false ?
                                        (<CancelButton onClick={() => handleDeleteClick(x)} enabled={true}
                                                       label={t('button.cancel')}/>)
                                        :
                                        (<ValidationButton onClick={() => handleConsumeClick(x)}
                                                           enabled={x.consumedAt === null} label={t('validate_action')}/>)
                                    }
                                </Col>
                            </Row>)
                    }
                )}
            </>
        )
    };

    const renderListXs = () => {
        return (
            <>
                {timeSlotAppointments.map((x, i) => {
                        const user = x.details !== "" ? JSON.parse(x.details) : {};
                        const bg = i % 2 === 0 ? styles.bgOne : styles.bgTwo;
                        return (
                            <Row className="mb-2 mt-1 align-items-center justify-content-center align-self-center" key={i}
                                 style={bg}>
                                <Col xs={12} xl={1}/>
                                <Col xs={6} xl={2}> <TextField value={user.firstName} label={t('first_name')} type="text"
                                                               disabled
                                                               InputLabelProps={styles.input} inputProps={styles.input}
                                                               className="w-100"/>
                                </Col>
                                <Col xs={6} xl={2}> <TextField value={user.lastName} label={t('last_name')} type="text"
                                                               disabled
                                                               InputLabelProps={styles.input} inputProps={styles.input}
                                                               className="w-100"/>
                                </Col>
                                <Col xs={6} xl={1}> <TextField value={user.phoneNumber} label={t('phone_number')}
                                                               type="text" disabled
                                                               InputLabelProps={styles.input} inputProps={styles.input}
                                                               className="w-100"/>
                                </Col>
                                <Col xs={6} xl={2}> <TextField value={user.email} label={t('email')} type="text" disabled
                                                               InputLabelProps={styles.input} inputProps={styles.input}
                                                               className="w-100"/>
                                </Col>
                                <Col xs={4} xl={1}> <TextField value={x.placeNumber} label={t('place')} type="text" disabled
                                                               InputLabelProps={styles.input} inputProps={styles.input}
                                                               className="w-100"/>
                                </Col>
                                <Col xs={4} xl={1}> <TextField value={format(Date.parse(x.createdAt), t('date_format'))}
                                                               label={t('creation_date')} type="text" disabled
                                                               InputLabelProps={styles.input} inputProps={styles.input}
                                                               className="w-100"/>
                                </Col>
                                <Col xs={4} xl={1}> <TextField
                                    value={x.consumedAt === null ? "" : format(Date.parse(x.consumedAt), t('date_format'))}
                                    label={t('used_date')} type="text" disabled
                                    InputLabelProps={styles.input} inputProps={styles.input}
                                    className="w-100"/>
                                </Col>
                                <Col xs={12} xl={1} className="mt-1 mt-xl-0">
                                    {timeSlot?.isPassed === false ?
                                        (<CancelButton onClick={() => handleDeleteClick(x)} enabled={true}
                                                       label={t('button.cancel')}/>)
                                        :
                                        (<ValidationButton onClick={() => handleConsumeClick(x)}
                                                           enabled={x.consumedAt === null} label={t('validate_action')}/>)
                                    }
                                </Col>
                            </Row>)
                    }
                )}
            </>
        )
    };

    return (
        <main>
            {timeSlotAppointments.length > 0 && renderList()}
            {timeSlot.isAvailable && Date.now() < Date.parse(timeSlot.startDateTime) && frameContainer()}
        </main>
    )
};

AppointmentContent.propTypes = {
    timeSlot: PropTypes.object.isRequired,
};


const styles = {
    input: {
        style: {
            fontSize: 13,
            fontFamily: FONTS.Regular
        },
        step: 600,
    },
    addButton: {
        backgroundColor: Colors.lightblue,
        color: Colors.white,
        fontSize: 12,
        fontFamily: FONTS.Regular
    },
    deleteButton: {
        backgroundColor: Colors.grey,
        color: Colors.white,
        fontSize: 10,
        fontFamily: FONTS.Regular
    },
    bgOne: {
        fontFamily: FONTS.Regular,
        fontSize: 11,
        backgroundColor: Colors.greyhish
    },
    bgTwo: {
        fontFamily: FONTS.Regular,
        fontSize: 11
    },
};


export default AppointmentContent;

