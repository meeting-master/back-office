import React, {useEffect, useState} from 'react';
import {Col, Row} from "react-bootstrap";
import {useTranslation} from "react-i18next";
import {Button, MenuItem, TextField} from '@material-ui/core';
import {AddCircle, Delete} from "@material-ui/icons";
import Select from "react-select";
import PropTypes from "prop-types";

//services
import useRematchDispatch from "../../hooks/useRematchDispatch";
import Colors from "../../utils/Colors";
import FONTS from "../../utils/fonts";

//components
import {CancelButton, ValidationButton} from "../../components/Buttons";


const ScheduleContent = ({offices, services}) => {
    const {t} = useTranslation();
    const {SetSchedule} = useRematchDispatch(dispatch => ({
        SetSchedule: dispatch.calendar.SetSchedule,
    }));

    const daysOfWeek = [
        {value: 0, label: t('sunday')},
        {value: 1, label: t('monday')},
        {value: 2, label: t('tuesday')},
        {value: 3, label: t('wednesday')},
        {value: 4, label: t('thursday')},
        {value: 5, label: t('friday')},
        {value: 6, label: t('saturday')}
    ];

    //const ongoing = useSelector(state => state.calendar.ongoing);

    const [workDays, setWorkDays] = useState([]);
    const [timeFrames, setTimeFrames] = useState([]);
    const [error, setError] = useState(false);
    const [duration, setDuration] = useState(0);
    const [place, setPlace] = useState(1);
    const [times, setTimes] = useState({startTime: {valueStr: "00:00"}, endTime: {valueStr: "00:00"}});
    const [officeId, setOfficeId] = useState('');
    const [serviceId, setServiceId] = useState();
    const [officeServices, setOfficeServices] = useState([]);


    useEffect(() => {
        let servs = [];
        const office = offices.find(x => x.id === officeId);
        if (office) {
            const os = office.serviceIds;
            services.forEach((s) => {
                if (os.includes(s.id)) {
                    servs.push(s);
                }
            })
        }
        setOfficeServices(servs);
        // eslint-disable-next-line
    }, [officeId]);

    const onDaysOfWeekSelected = (ids) => {
        const v = ids.map(x => x.value);
        setWorkDays(v);
    };

    const handleAddClick = () => {
        if (times.startTime.value === undefined || times.endTime.value === undefined ||
            times.startTime.value >= times.endTime.value || duration <= 0 || place <= 0) {
            setError(true);
            return;
        }
        const newValue = {startTime: times.startTime.value, endTime: times.endTime.value, duration, place};
        setTimeFrames([...timeFrames, newValue]);
        initValues();
    };

    const handleDeleteClick = (index) => {
        const newValues = timeFrames;
        newValues.splice(index, 1);
        setTimeFrames([...newValues]);
    };

    const initValues = () => {
        setTimes({startTime: {valueStr: "00:00"}, endTime: {valueStr: "00:00"}});
        setDuration(0);
        setPlace(1);
    };

    const enableSave = () => {
        return serviceId && officeId && workDays.length > 0 && timeFrames.length > 0;
    };

    const handleSubmitClick = () => {
        const inputs = {officeId, serviceId, workDays, timeFrames};
        SetSchedule(inputs);
    };

    const handleResetClick = () => {
        setTimeFrames([]);
        initValues();
    };

    const onTimeSelected = (event) => {
        const valueStr = event.target.value;
        const values = valueStr.split(':');
        const value = parseInt(values[0]) * 60 + parseInt(values[1]);
        setTimes({...times, [event.target.id]: {value, valueStr}});
    };

    const intParseTime = (time) => {
        const hour = parseInt(time / 60);
        const min = time % 60;
        return `${hour}:${min}`
    };


    const frameContainer = () => {
        return (
            <Row className="text-center mb-2">
                <Col xs={12} md={1}/>
                <Col xs={3} md={2}>
                    <TextField id="startTime" value={times.startTime.valueStr} label={t('start_time')} type="time"
                               InputLabelProps={styles.input} inputProps={styles.input} error={error}
                               onChange={(event => onTimeSelected(event))} className="w-100"/>
                </Col>
                <Col xs={3} md={2}>
                    <TextField id="endTime" label={t('end_time')} type="time" value={times.endTime.valueStr}
                               InputLabelProps={styles.input} inputProps={styles.input} error={error}
                               onChange={(event => onTimeSelected(event))} className="w-100"/>
                </Col>
                <Col xs={3} md={2}>
                    <TextField id="duration" type="number" label={t('timeslot_duration')} error={error}
                               onChange={(event) => setDuration(parseInt(event.target.value))}
                               value={duration} size="medium" className="w-100" InputProps={styles.input}
                               InputLabelProps={styles.input}/>
                </Col>
                <Col xs={3} md={2}>
                    <TextField id="place" type="number" label={t('available_place')} error={error}
                               onChange={(event) => setPlace(parseInt(event.target.value))}
                               value={place} size="medium" className="w-100" InputProps={styles.input}
                               InputLabelProps={styles.input}/>
                </Col>
                <Col xs={12} md={2}>
                    <Button startIcon={<AddCircle/>} size="small" className="w-100 loweCase mt-2"
                            style={styles.addButton} disabled={workDays.length === 0} onClick={() => {
                        handleAddClick()
                    }}>
                        {t('button.add_schedule')}
                    </Button>
                </Col>
            </Row>
        )
    };

    const changeContainer = () => {
        return (
            <>
                {timeFrames.map((tf, i) =>
                    <Row className="text-center mb-2" key={i}>
                        <Col xs={12} md={1}/>
                        <Col xs={3} md={2}>
                            <TextField value={intParseTime(tf.startTime)} label={t('start_time')} type="text" disabled
                                       InputLabelProps={styles.input} inputProps={styles.input}
                                       className="w-100"/>
                        </Col>
                        <Col xs={3} md={2}>
                            <TextField value={intParseTime(tf.endTime)} label={t('end_time')} type="text" disabled
                                       InputLabelProps={styles.input} inputProps={styles.input}
                                       className="w-100"/>
                        </Col>
                        <Col xs={3} md={2}>
                            <TextField value={tf.duration} label={t('timeslot_duration')} type="text" disabled
                                       InputLabelProps={styles.input} inputProps={styles.input}
                                       className="w-100"/>
                        </Col>
                        <Col xs={3} md={2}>
                            <TextField value={tf.place} label={t('available_place')} type="text" disabled
                                       InputLabelProps={styles.input} inputProps={styles.input}
                                       className="w-100"/>
                        </Col>
                        <Col xs={12} md={2}>
                            <Button startIcon={<Delete/>} variant="outline-dark" size="small"
                                    className="w-100 loweCase mt-2"
                                    style={styles.deleteButton} onClick={() => {
                                handleDeleteClick(i)
                            }}>
                                {t('button.delete_schedule')}
                            </Button>
                        </Col>
                    </Row>
                )}
            </>
        )
    };

    return (
        <main>
            <Row className="text-center mb-2">
                <Col xs={12} md={1}/>
                <Col xs={12} md={3}>
                    <TextField id="officeId" select label={t('office')} value={officeId} required
                               size="medium" className="w-100" InputProps={styles.input} InputLabelProps={styles.input}
                               error={false}
                               onChange={(event) => setOfficeId(event.target.value)}>

                        {offices.filter(x => x.isArchived === false).map((x) => (
                            <MenuItem key={x.id} value={x.id} style={{fontSize: 10}}>
                                {x.name}
                            </MenuItem>
                        ))}
                    </TextField>
                </Col>

                <Col xs={12} md={3}>
                    <TextField id="serviceId" select label={t('service')} value={serviceId} required
                               size="medium" className="w-100" InputProps={styles.input} InputLabelProps={styles.input}
                               error={false}
                               onChange={(event) => setServiceId(event.target.value)}>

                        {officeServices.filter(x => x.isArchived !== true).map((x) => (
                            <MenuItem key={x.id} value={x.id} style={{fontSize: 10}}>
                                {x.name}
                            </MenuItem>
                        ))}
                    </TextField>
                </Col>
                <Col xs={12} md={4}>
                    <Select options={daysOfWeek} isClearable isMulti closeMenuOnSelect={false}
                            isDisabled={serviceId === undefined}
                            styles={customStyles} placeholder={t('week_days')} onChange={onDaysOfWeekSelected}/>
                </Col>
                <Col xs={12}/>
            </Row>
            {changeContainer()}
            {frameContainer()}
            <Row className="mt-4 mb-4">
                <Col xs={12} md={1}/>
                <Col xs={8} md={3}>
                    <ValidationButton onClick={handleSubmitClick} enabled={enableSave()}
                                      label={t('button.save_schedule')}/>
                </Col>
                <Col xs={4} md={3}>
                    <CancelButton onClick={handleResetClick}/>
                </Col>
                <Col xs={12} md={4}/>
            </Row>

        </main>
    )
};

ScheduleContent.propTypes = {
    customerId: PropTypes.string.isRequired,
    services: PropTypes.array,
    offices: PropTypes.array,
};

ScheduleContent.defaultProps = {
    customerId: "",
    locations: [],
    services: [],
};

const styles = {
    input: {
        style: {
            fontSize: 13,
            fontFamily: FONTS.Regular
        },
        step: 600,
    },
    addButton: {
        backgroundColor: Colors.black,
        color: Colors.white,
        fontSize: 12,
        fontFamily: FONTS.Regular
    },
    deleteButton: {
        backgroundColor: Colors.grey,
        color: Colors.white,
        fontSize: 10,
        fontFamily: FONTS.Regular
    }
};
const customStyles = {
    control: styles => ({
        ...styles,
        borderRadius: 0,
        marginTop: 5,
        fontSize: 12,
        borderWidth: 0,
        borderBottomWidth: 1,
        borderBottomColor: '#949494',
    }),
    multiValue: (styles) => {
        return {
            ...styles,
            backgroundColor: 'white',
            fontSize: 12,
        };
    },
    option: (styles) => {
        return {
            ...styles,
            fontSize: 12,
            fontFamily: FONTS.Regular,
        }
    }
};

export default ScheduleContent
