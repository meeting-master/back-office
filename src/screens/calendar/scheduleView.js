import React, {useEffect, useState} from 'react';
import {useSelector} from "react-redux";
import {Col, Row} from "react-bootstrap";
import {useTranslation} from "react-i18next";
import {Button, MenuItem, TextField} from '@material-ui/core';
import {AddCircle, Delete} from "@material-ui/icons";
import PropTypes from "prop-types";

//services
import useRematchDispatch from "../../hooks/useRematchDispatch";
import Colors from "../../utils/Colors";
import FONTS from "../../utils/fonts";

//components
import {CancelButton, ValidationButton} from "../../components/Buttons";


const ScheduleView = ({officeId, serviceId}) => {
    const {t} = useTranslation();
    const {UpdateSchedule, GetSchedule} = useRematchDispatch(dispatch => ({
        GetSchedule: dispatch.calendar.GetSchedule,
        UpdateSchedule: dispatch.calendar.UpdateSchedule,
    }));

    const daysOfWeek = [
        {value: 0, label: t('sunday')},
        {value: 1, label: t('monday')},
        {value: 2, label: t('tuesday')},
        {value: 3, label: t('wednesday')},
        {value: 4, label: t('thursday')},
        {value: 5, label: t('friday')},
        {value: 6, label: t('saturday')}
    ];

    //const ongoing = useSelector(state => state.calendar.ongoing);
    const schedules = useSelector(state => state.calendar.schedules);

    const [timeFrames, setTimeFrames] = useState([]);
    const [error, setError] = useState(false);
    const [duration, setDuration] = useState(0);
    const [place, setPlace] = useState(1);
    const [times, setTimes] = useState({startTime: {valueStr: "00:00"}, endTime: {valueStr: "00:00"}});
    const [workDay, setWorkDay] = useState();



    useEffect(() => {
        GetSchedule({officeId, serviceId});
        // eslint-disable-next-line
    }, [officeId, serviceId]);

    useEffect(() => {
        setTimeFrames([...schedules]);
    }, [schedules]);


    const handleAddClick = () => {
        if (!enableAdd()) {
            setError(true);
            return;
        }
        const newValue = {workDay, startTime: times.startTime.value, endTime: times.endTime.value, duration, place};
        setTimeFrames([...timeFrames, newValue]);
        initValues();
    };

    const handleDeleteClick = (index) => {
           const newValues = timeFrames;
           newValues.splice(index, 1);
           setTimeFrames([...newValues]);
    };

    const initValues = () => {
        setTimes({startTime: {valueStr: "00:00"}, endTime: {valueStr: "00:00"}});
        setDuration(0);
        setPlace(1);
    };

    const enableAdd = () => {
        return !(times.startTime.value === undefined || times.endTime.value === undefined ||
            times.startTime.value >= times.endTime.value || duration <= 0 || place <= 0 || workDay < 0 || workDay > 6);
    };

    const handleSubmitClick = () => {
        const inputs = {officeId, serviceId, timeFrames};
        UpdateSchedule(inputs);
    };

    const handleResetClick = () => {
        setTimeFrames([]);
        initValues();
    };

    const onTimeSelected = (event) => {
        const valueStr = event.target.value;
        const values = valueStr.split(':');
        const value = parseInt(values[0]) * 60 + parseInt(values[1]);
        setTimes({...times, [event.target.id]: {value, valueStr}});
    };

    const intParseTime = (time) => {
        const hour = parseInt(time / 60);
        const min = time % 60;
        return `${hour}:${min}`
    };


    const newContainer = () => {
        return (
            <Row className="text-center mb-2">
                <Col xs={12} md={2}>
                <TextField value={workDay} label={t('day')} type="number"
                           InputProps={styles.input} InputLabelProps={styles.input}
                           className="w-100" select
                           onChange={(event) => setWorkDay(event.target.value)}>
                    {daysOfWeek.map((day) => (
                        <MenuItem key={day.value} value={day.value} style={{fontSize: 10}}>
                            {day.label}
                        </MenuItem>
                    ))}
                </TextField>
            </Col>
                <Col xs={3} md={2}>
                    <TextField id="startTime" value={times.startTime.valueStr} label={t('start_time')} type="time"
                               InputLabelProps={styles.input} inputProps={styles.input} error={error}
                               onChange={(event => onTimeSelected(event))} className="w-100"/>
                </Col>
                <Col xs={3} md={2}>
                    <TextField id="endTime" label={t('end_time')} type="time" value={times.endTime.valueStr}
                               InputLabelProps={styles.input} inputProps={styles.input} error={error}
                               onChange={(event => onTimeSelected(event))} className="w-100"/>
                </Col>
                <Col xs={3} md={2}>
                    <TextField id="duration" type="number" label={t('timeslot_duration')} error={error}
                               onChange={(event) => setDuration(parseInt(event.target.value))}
                               value={duration} size="medium" className="w-100" InputProps={styles.input}
                               InputLabelProps={styles.input}/>
                </Col>
                <Col xs={3} md={2}>
                    <TextField id="place" type="number" label={t('available_place')} error={error}
                               onChange={(event) => setPlace(parseInt(event.target.value))}
                               value={place} size="medium" className="w-100" InputProps={styles.input}
                               InputLabelProps={styles.input}/>
                </Col>
                <Col xs={12} md={2}>
                    <Button startIcon={<AddCircle/>} variant="outline-dark" size="small" className="w-100 loweCase mt-2"
                            style={styles.addButton} disabled={!enableAdd()} onClick={() => {
                        handleAddClick()
                    }}>
                        {t('button.add_schedule')}
                    </Button>
                </Col>
            </Row>
        )
    };

    const scheduleContainer = () => {
        return (
            <>
                {timeFrames.map((tf, i) =>
            <Row className="text-center editZone p-2" key={i}>
                <Col xs={4} md={2}>
                <TextField value={daysOfWeek[tf.workDay].label} label={t('day')} type="text" disabled
                           InputLabelProps={styles.input} inputProps={styles.input}
                           className="w-100"/>
                </Col>
                <Col xs={4} md={2}>
                    <TextField value={intParseTime(tf.startTime)} label={t('start_time')} type="text" disabled
                               InputLabelProps={styles.input} inputProps={styles.input}
                               className="w-100"/>
                </Col>
                <Col xs={4} md={2}>
                    <TextField value={intParseTime(tf.endTime)} label={t('end_time')} type="text" disabled
                               InputLabelProps={styles.input} inputProps={styles.input}
                               className="w-100"/>
                </Col>
                <Col xs={4} md={2}>
                    <TextField value={tf.duration} label={t('timeslot_duration')} type="text" disabled
                               InputLabelProps={styles.input} inputProps={styles.input}
                               className="w-100"/>
                </Col>
                <Col xs={4} md={2}>
                    <TextField value={tf.place} label={t('available_place')} type="text" disabled
                               InputLabelProps={styles.input} inputProps={styles.input}
                               className="w-100"/>
                </Col>
                <Col xs={4} md={2}>
                    <Button startIcon={<Delete/>} variant="outline-dark" size="small" className="w-100 loweCase mt-2"
                            style={styles.deleteButton}  onClick={() => {
                        handleDeleteClick(i)
                    }}>
                        {t('button.delete_schedule')}
                    </Button>
                </Col>
            </Row>
                )}
            </>
        )
    };

    return (
        <main>
            {scheduleContainer()}
            {newContainer()}
            <Row className="mt-4 mb-4">
                <Col xs={12} md={1}/>
                <Col xs={8} md={3}>
                    <ValidationButton onClick={handleSubmitClick} enabled={true}  label={t('button.save_modification')}/>
                </Col>
                <Col xs={4} md={3}>
                   <CancelButton onClick={handleResetClick}/>
                </Col>
                <Col xs={12} md={4}/>
            </Row>

        </main>
    )
};

ScheduleView.propTypes = {
    officeId: PropTypes.string.isRequired,
    serviceId: PropTypes.string.isRequired,
};

const styles = {
    input: {
        style: {
            fontSize: 13,
            fontFamily: FONTS.Regular
        },
        step: 600,
        shrink: true,
    },
    addButton: {
        backgroundColor: Colors.black,
        color: Colors.white,
        fontSize: 10,
        fontFamily: FONTS.Regular,
        justifyContent: 'flex-start',
    },
    deleteButton: {
        backgroundColor: Colors.clearGray,
        color: Colors.black,
        fontSize: 10,
        fontFamily: FONTS.Regular,
        justifyContent: 'flex-start',
    }
};

export default ScheduleView
