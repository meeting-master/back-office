import React, {useEffect} from 'react';
import {useLocation} from 'react-router-dom';
import PropTypes from 'prop-types';
import {Box, Divider, Drawer, Hidden, List, makeStyles} from '@material-ui/core';
import {BarChart as BarChartIcon, Clock, Settings as SettingsIcon, UserPlus as UserPlusIcon} from 'react-feather';
import NavItem from './NavItem';
import Colors from '../../utils/Colors';
import {AccountCircle, Alarm, Business, CalendarToday, FlagSharp} from "@material-ui/icons";


const items = [
    {
        icon: BarChartIcon,
        title: 'menu_dashboard',
        eventKey: 'menu_dashboard',
    },
    {
        icon: FlagSharp,
        title: 'menu_service',
        eventKey: 'menu_service',
    },
    {
        icon: Business,
        title: 'menu_office',
        eventKey: 'menu_office'
    },
    {
        icon: Clock,
        title: 'menu_schedule',
        eventKey: 'menu_schedule',
    },
    {
        icon: CalendarToday,
        title: 'menu_calendar',
        eventKey: 'menu_calendar',
    },
    {
        icon: Alarm,
        title: 'menu_appointment',
        eventKey: 'menu_appointment',
    },

    {
        icon: SettingsIcon,
        title: 'menu_settings',
        eventKey: 'menu_settings',
    },
    {
        icon: UserPlusIcon,
        title: 'menu_manage_user',
        eventKey: 'menu_create_user',
    },

    {
        icon: AccountCircle,
        title: 'menu_profile',
        eventKey: 'menu_profile',
    },

];

const useStyles = makeStyles(() => ({
    mobileDrawer: {
        width: 240,
        backgroundColor: Colors.meanBlue,
    },
    desktopDrawer: {
        width: 240,
        top: 50,
        height: 'calc(100% - 50px)',
        backgroundColor: Colors.meanBlue,
    },
    avatar: {
        cursor: 'pointer',
        width: 64,
        height: 64
    }
}));

const NavBar = ({onMobileClose, openMobile, onSelected, selectedKey}) => {
    const classes = useStyles();
    const location = useLocation();

    useEffect(() => {
        if (openMobile && onMobileClose) {
            onMobileClose();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [location.pathname]);

    const content = (
        <Box
            height="100%"
            display="flex"
            flexDirection="column"
        >

            <Divider/>
            <Box p={2}>
                <List>
                    {items.map((item) => (
                        <NavItem
                            className="mb-2"
                            key={item.title}
                            title={item.title}
                            icon={item.icon}
                            items={item.items}
                            onSelected={onSelected}
                            eventKey={item.eventKey}
                            isSelected={item.eventKey === selectedKey}
                            closeMobile={onMobileClose}
                        />
                    ))}
                </List>
            </Box>
            <Box flexGrow={1}/>
        </Box>
    );

    return (
        <>
            <Hidden lgUp>
                <Drawer
                    anchor="left"
                    classes={{paper: classes.mobileDrawer}}
                    onClose={onMobileClose}
                    open={openMobile}
                    variant="temporary"
                >
                    {content}
                </Drawer>
            </Hidden>
            <Hidden mdDown>
                <Drawer
                    anchor="left"
                    classes={{paper: classes.desktopDrawer}}
                    open
                    variant="persistent"
                >
                    {content}
                </Drawer>
            </Hidden>
        </>
    );
};

NavBar.propTypes = {
    onMobileClose: PropTypes.func,
    openMobile: PropTypes.bool
};

NavBar.defaultProps = {
    onMobileClose: () => {
    },
    openMobile: false
};

export default NavBar;
