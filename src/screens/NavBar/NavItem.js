import React, {useState} from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';
import {Button, ListItem, makeStyles} from '@material-ui/core';
import Collapse from "@material-ui/core/Collapse";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";

import {useTranslation} from "react-i18next";
import Colors from "../../utils/Colors";

const useStyles = makeStyles((theme) => ({
    item: {
        display: 'flex',
        paddingTop: 0,
        paddingBottom: 0
    },
    button: {
        fontWeight: theme.typography.fontWeightMedium,
        justifyContent: 'flex-start',
        letterSpacing: 0,
        padding: '10px 8px',
        textTransform: 'none',
        width: '100%'
    },
    icon: {
        marginRight: theme.spacing(1)
    },
    title: {
        marginRight: 'auto',
        color: Colors.white,
        fontSize: 14,
    },
    subTitle: {
        marginLeft: theme.spacing(1),
        color: Colors.white
    },
    active: {
        color: theme.palette.primary.main,
        '& $title': {
            fontWeight: theme.typography.fontWeightMedium
        },
        '& $icon': {
            color: 'black',
        }
    },
    isSelected: {
        backgroundColor: Colors.white,
    }
}));

const NavItem = ({onSelected, className, href, icon: Icon, title, eventKey, isSelected, items = [], closeMobile, ...rest}) => {
    const {t} = useTranslation();
    const classes = useStyles();
    const isExpandable = items && items.length > 0;
    const [open, setOpen] = useState(true);
    const selected = isSelected ? {backgroundColor: Colors.blueLight} : {};
    const selectedIcon = isSelected ? {color: Colors.white} : {color: Colors.blueLight};

    const handleClick = (key) => {
        if (key) {
            onSelected(key);
            setOpen(!open);
        } else {
            if (items.length > 0) {
                setOpen(!open);
            } else {
                onSelected(eventKey);
                setOpen(!open);
            }
        }
        closeMobile();

    };

    const MenuItemChildren = () => isExpandable ? (
        <Collapse in={open} timeout="auto" unmountOnExit>
            <Divider/>
            <List component="div" disablePadding style={{backgroundColor: Colors.royalblue}}>
                {items.map((item, index) => (
                    <SubMenuItem {...item} key={index}/>
                ))}
            </List>
        </Collapse>
    ) : null;

    const SubMenuItem = (item) => (
        <ListItem
            className={clsx(classes.item, className)}
            disableGutters
            {...rest}
        >
            <Button
                className={classes.button}
                onClick={() => handleClick(item.eventKey)}
            >
          <span className={classes.subTitle}>
          {t(item.eventKey)}
        </span>
            </Button>
        </ListItem>
    );

    const MenuItem = () => (
        <ListItem
            className={clsx(classes.item, className)}
            disableGutters
            {...rest}
        >
            <Button
                className={classes.button}
                onClick={() => handleClick()}
                style={selected}
            >
                {Icon && (
                    <Icon
                        className={classes.icon}
                        size="20"
                        style={selectedIcon}
                    />
                )}
                <span className={classes.title}>
          {t(title)}
        </span>
            </Button>
        </ListItem>
    );
    return (
        <>
            {MenuItem()}
            {MenuItemChildren()}
        </>
    )
};

NavItem.propTypes = {
    className: PropTypes.string,
    href: PropTypes.string,
    icon: PropTypes.elementType,
    title: PropTypes.string,
    onSelected: PropTypes.func,
};

export default NavItem;
