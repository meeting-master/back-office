import React, {useEffect, useState} from 'react';
import {useSelector} from "react-redux";
import {Button, Col, Container, Row, Spinner} from "react-bootstrap";
import '../../css/login.css'
//hooks
import useRematchDispatch from "../../hooks/useRematchDispatch";
import {useTranslation} from "react-i18next";
//services
import Colors from "../../utils/Colors";
//components
import {AlertMessage, Alerts} from '../../components/alert';
import trame2 from "../../images/trame2.png";
import logoBlue from "../../images/logoBlue.png";
import TextField from "@material-ui/core/TextField";
import {AppStyles} from "../../css/styles";
import Footer from "../../components/Footer";
import Helmet from "react-helmet";

const EmailFormat = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;


const RegisterScreen = (props) => {
    const {t} = useTranslation();
    const {history} = props;
    const {CreateAccount, GetLogos} = useRematchDispatch(dispatch => ({
        CreateAccount: dispatch.customer.CreateAccount,
        GetLogos: dispatch.customer.GetLogos,
    }));

    const error = useSelector(state => state.customer.error);
    const ongoing = useSelector(state => state.customer.ongoing);
    const [errors, setErrors] = useState({
        name: false,
        activities: false,
        email: false,
        password: false
    });
    const [inputs, setInputs] = useState({
        name: "",
        activities: "",
        username: "",
        password: "",
        partnerCode: ""
    });
    const [alert, setAlert] = useState({
        show: false,
        message: "",
        heading: "",
        type: ""
    });

    const [init, setInit] = useState(true);

    useEffect(() => {
        GetLogos();
        // eslint-disable-next-line
    }, []);

    useEffect(() => {
        if (error !== null) {
            if (error.message !== null) {
                setAlert(a => AlertMessage(t(error.message), error.id, "danger"));
            } else if (init === false) {
                setAlert(a => AlertMessage(t('register_success_message'), Date.now(), "success"));
            }
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [error]);

    useEffect(() => {
        setAlert(al => ({...al, show: false}));
    }, []);

    const onCloseAlert = () => {
        setAlert(a => ({...a, "show": false}));
        if (error.message === null) {
            history.push("/login")
        }
    };

    const onPropertyValueChange = (event) => {
        setInputs({...inputs, [event.target.id]: event.target.value});
    };

    const handleClick = () => {
        if (inputs.name.length < 2) {
            setErrors(errors => ({...errors, "name": true}));
            return;
        } else {
            setErrors(errors => ({...errors, "name": false}));
        }
        if (inputs.activities.length < 2) {
            setErrors(errors => ({...errors, "activities": true}));
            return;
        } else {
            setErrors(errors => ({...errors, "name": false}));
        }
        if (!EmailFormat.test(inputs.username)) {
            setErrors(errors => ({...errors, "email": true}));
            return
        } else {
            setErrors(errors => ({...errors, "email": false}));
        }
        if (inputs.password.length < 4) {
            setErrors(errors => ({...errors, "password": true}));
            return;
        } else {
            setErrors(errors => ({...errors, "password": false}));
        }
        setInit(false);
        CreateAccount(inputs);
    };

    return (
        <div>
            <Helmet>
                <title>{t('register_title')}</title>
            </Helmet>
            <Container fluid style={styles.root}>
                <Row style={styles.middle}>
                    <Col className="d-none d-md-block" md={6} xl={8}>
                        <Row className="land-h">
                            <Col className="my-auto">
                                <div style={styles.image}>
                                    <img src={trame2} alt={'Easy Meeting'} width={'40%'}/>
                                </div>
                            </Col>
                        </Row>

                    </Col>
                    <Col xs={12} md={6} xl={4} className="loginContainer p-2 p-xl-5 h-100 scrollable"
                         style={styles.container}>
                        <Row>
                            <Col sx={12} className="mb-3 mt-5 land-content" style={styles.image}>
                                <img src={logoBlue} alt={'Easy Meeting'} width="30%" className="land-image"/>
                            </Col>
                            <Col xs={12}>
                                <TextField type="text"
                                           placeholder={t('business_name')}
                                           id="name"
                                           error={errors.name}
                                           onChange={(event) => onPropertyValueChange(event)}
                                           style={AppStyles.loginInput} size="medium" className="w-100 land-content"
                                           InputProps={AppStyles.input} InputLabelProps={AppStyles.input}/>
                            </Col>
                            <Col xs={12} className="mt-2 land-mt">
                                <TextField type="text"
                                           placeholder={t('customer_activities')}
                                           id="activities"
                                           error={errors.activities}
                                           onChange={(event) => onPropertyValueChange(event)}
                                           style={AppStyles.loginInput} size="medium" className="w-100 land-content"
                                           InputProps={AppStyles.input} InputLabelProps={AppStyles.input}/>
                            </Col>
                            <Col xs={12} className="mt-2 land-mt">
                                <TextField type="text"
                                           placeholder={t('email')}
                                           id="username"
                                           error={errors.email}
                                           onChange={(event) => onPropertyValueChange(event)}
                                           style={AppStyles.loginInput} size="medium" className="w-100 land-content"
                                           InputProps={AppStyles.input} InputLabelProps={AppStyles.input}/>
                            </Col>
                            <Col xs={12} className="mt-2 land-mt">
                                <TextField type="password"
                                           placeholder={t('password')} id="password"
                                           error={errors.password}
                                           onChange={(event) => onPropertyValueChange(event)}
                                           style={AppStyles.loginInput}
                                           size="medium" className="w-100 land-content"
                                           InputProps={AppStyles.input} InputLabelProps={AppStyles.input}/>
                            </Col>
                            {/*

                            <Col xs={12}>
                                <TextField type="number"
                                           placeholder={t('partner_code')}
                                           id="partnerCode"
                                           onChange={(event) => onPropertyValueChange(event)}
                                           style={AppStyles.loginInput} size="medium" className="w-100 land-content"
                                           InputProps={AppStyles.input} InputLabelProps={AppStyles.input}/>
                            </Col>*/}
                            <Col xs={12}>
                                <div className="mt-4" style={AppStyles.loginInput}>
                                    <Button onClick={() => {
                                        handleClick()
                                    }} className="w-100" style={{backgroundColor: Colors.lightblue}}> {ongoing ?
                                        <Spinner as="span" animation="grow" size="sm" role="status"
                                                 aria-hidden="true"/> : null} {t('register_your_account')}</Button>
                                </div>
                            </Col>
                            <Col xs={12}>
                                <div className="mt-4" style={AppStyles.loginInput}>
                                    <Button variant="outline-dark" style={AppStyles.linkButton}
                                            onClick={() => history.push('/login')}
                                            className="w-100">{t('login')}</Button>
                                </div>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Footer/>

                <Alerts show={alert.show} type={alert.type} message={alert.message} heading={alert.heading}
                        onClose={onCloseAlert}/>
            </Container>
        </div>
    );
};

const styles = {
    root: {
        display: 'flex',
        flexDirection: 'column',
        height: '100vh',
        backgroundColor: Colors.white,
    },
    middle: {
        backgroundColor: Colors.meanBlue,
        height: '80%',
    },
    image: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: '100%',
    },
    container: {
        backgroundColor: Colors.white,
    },
};

export default RegisterScreen
