import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {useSelector} from "react-redux";
import {Button, Spinner, Row, Col} from "react-bootstrap";

import {Chip, Checkbox, TextField} from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete";
import LocationOnIcon from '@material-ui/icons/LocationOn';

//hooks
import useRematchDispatch from "../../hooks/useRematchDispatch";
import {useTranslation} from "react-i18next";
//components
import {AppStyles} from "../../css/styles";
import FONTS from "../../utils/fonts";
import Colors from "../../utils/Colors";


const OfficeContent = ({customerId, locations, services, office, defaultOptions}) => {
    const {t} = useTranslation();
    const {UpSertOffice} = useRematchDispatch(dispatch => ({
        UpSertOffice: dispatch.customer.UpSertOffice,
    }));
    const ongoing = useSelector(state => state.customer.ongoing);
    const [inputs, setInputs] = useState({
        name: '',
        alias: '',
        streetNumber: '',
        streetName: '',
        zipCode: '',
        phoneNumber: '',
        webSite: '',
        email: '',
        longitude: 0,
        latitude: 0,
        districtId: '',
        serviceIds: [],
        isArchived: false
    });

    const [errors, setErrors] = useState({
        name: false,
        phoneNumber: false,
        location: false,
        service: false,
    });

    const [optionValue, setOptionValue] = useState(null);
    const [serviceOptions, setServiceOptions] = useState([]);

    useEffect(() => {
        if (office) {
            setInputs(office);
            initServiceOptions();
        }
        // eslint-disable-next-line
    }, [office]);


    const initServiceOptions = () => {
        let defaults = [];
        if (office && services && services.length > 0) {
            services.map(opt => {
                if (office.serviceIds && office?.serviceIds.includes(opt.id)) {
                    defaults.push(opt);
                }
            });
        }
        setServiceOptions(defaults)
    };


    useEffect(() => {
        if (inputs.districtId) {
            const v = locations.find(x => x.id === inputs.districtId);
            if (v) {
                setOptionValue(v)
            }
        }
        // eslint-disable-next-line
    }, [inputs]);

    const onPropertyValueChange = (event) => {
        if (event.target.id === 'isArchived') {
            setInputs({...inputs, "isArchived": !inputs.isArchived});
            return;
        }
        let value = event.target.value;
        if (event.target.id === 'longitude' || event.target.id === 'latitude') {
            value = parseFloat(value);
        }
        setInputs({...inputs, [event.target.id]: value});
    };

    const handleClick = () => {
        if (inputs.name.length === 0) {
            setErrors(errs => ({...errs, "name": true}));
            return;
        } else {
            setErrors(errs => ({...errs, "name": false}));
        }
        if (inputs.serviceIds.length === 0) {
            setErrors(errs => ({...errs, "service": true}));
            return;
        } else {
            setErrors(errs => ({...errs, "service": false}));
        }

        if (inputs.districtId === null || inputs.districtId === "") {
            setErrors(errs => ({...errs, "location": true}));
            return;
        } else {
            setErrors(errs => ({...errs, "location": false}));
        }
        if (inputs.phoneNumber.length === 0) {
            setErrors(errs => ({...errs, "phoneNumber": true}));
            return;
        } else {
            setErrors(errs => ({...errs, "phoneNumber": false}));
        }

        inputs.customerId = customerId;
        UpSertOffice(inputs);
    };

    const onServiceSelected = (event, values) => {
        const ids = values.map(x => x.id);
        setServiceOptions(values);
        setInputs({...inputs, "serviceIds": ids});
    };

    const onLocationSelected = (event, value) => {
            if (value !== null) {
                setInputs({...inputs, "districtId": value.id});
            }
    };

    const handleCancelClick = () => {
        if (office) {
            setInputs(office);
        } else {
            let empty = {};
            for (const input in inputs) {
                let value = "";
                if (input === 'longitude' || input === 'latitude') {
                    value = 0;
                }
                if (input === 'districtId') {
                    value = [];
                }
                empty[input] = value;
            }
            setOptionValue(null);
            setInputs(empty);
        }
        initServiceOptions();
    };

    const displayLocations = () => (
    <Autocomplete
        id="location"
        onChange={onLocationSelected}
        value={optionValue}
        options={locations}
        getOptionLabel={(option) => option.location}
        renderInput={(params) =>
            <TextField {...params} label={t('location')} size="medium" required error={errors.location} InputLabelProps={AppStyles.input}
                       inputProps={{...params.inputProps, style: {fontSize: 12, fontFamily: FONTS.Regular},}} />}
        renderOption={(option) => <div>
            <LocationOnIcon size="small" style={{color: Colors.clearGray}} /> <span style={AppStyles.greyBarText}>{option.location}</span>
        </div>}
    />
    );

    const displayServices = () => (
        <Autocomplete
            id="ids"
            multiple
            onChange={onServiceSelected}
            options={services}
            value={serviceOptions}
            getOptionLabel={(option) => option.name}
            renderInput={(params) =>
                <TextField {...params} label={t('bound_services')} size="medium" required  error={errors.service} InputLabelProps={AppStyles.input}
                           inputProps={{...params.inputProps, style: {fontSize: 12, fontFamily: FONTS.Regular},}} />}
            renderOption={(option) => <span style={AppStyles.greyBarText}>{option.name}</span>}
            renderTags={(value, getTagProps) =>
                value.map((option, index) => (
                    <Chip
                        variant="outline"
                        label={option.name}
                        size="small"
                        {...getTagProps({ index })}
                        style={{fontSize: 12, fontFamily: FONTS.Regular, border: 0}}
                    />
                ))
            }
        />
    );
    return (
        <main className={office&&"editZone"}>
            <Row className="text-center">
                <Col xs={12} md={1}/>
                <Col xs={12} md={3}>
                    <TextField id="name" type="text" label={t('name')} error={errors.name} required
                               onChange={onPropertyValueChange}
                               value={inputs.name} size="medium" className="w-100" InputProps={AppStyles.input}
                               InputLabelProps={AppStyles.input}/>
                </Col>
                {services && (
                <Col xs={12} md={7}>
                    {displayServices()}
                </Col>)}
                <Col xs={12} md={1}/>
            </Row>
            <Row className="text-center mt-2">
                <Col xs={12} md={1}/>
                <Col xs={12} md={2}>
                    <TextField id="streetNumber" type="number" label={t('street_number')}
                               onChange={onPropertyValueChange}
                               value={inputs.streetNumber} size="medium" className="w-100"
                               InputProps={AppStyles.input} InputLabelProps={AppStyles.input}/>
                </Col>
                <Col xs={12} md={5}>
                    <TextField id="streetName" type="text" label={t('street_name')}
                               onChange={onPropertyValueChange}
                               value={inputs.streetName} size="medium" className="w-100" InputProps={AppStyles.input}
                               InputLabelProps={AppStyles.input}/>
                </Col>
                <Col xs={12} md={3}>
                    <TextField id="zipCode" type="text" label={t('zip_code')} onChange={onPropertyValueChange}
                               value={inputs.zipCode}
                               size="medium" className="w-100" InputProps={AppStyles.input}
                               InputLabelProps={AppStyles.input}/>
                </Col>
                <Col xs={12} md={1}/>
            </Row>
            <Row className="text-center mt-2">
                <Col xs={12} md={1}/>
                <Col xs={12} md={4}>
                    {displayLocations()}
                </Col>
                <Col xs={12} md={3}>
                    <TextField id="longitude" type="number" label={t('longitude')}
                               onChange={onPropertyValueChange}
                               value={inputs.longitude} size="medium" className="w-100" InputProps={AppStyles.input}
                               InputLabelProps={AppStyles.input}/>
                </Col>
                <Col xs={12} md={3}>
                    <TextField id="latitude" type="number" label={t('latitude')}
                               onChange={(event) => onPropertyValueChange(event)}
                               value={inputs.latitude} size="medium" className="w-100" InputProps={AppStyles.input}
                               InputLabelProps={AppStyles.input}/>
                </Col>
                <Col xs={12} md={1}/>
            </Row>
            <Row className="text-center mt-2">
                <Col xs={12} md={1}/>
                <Col xs={12} md={2}>
                    <TextField id="phoneNumber" type="text" label={t('phone_number')} error={errors.phoneNumber}
                               required
                               onChange={onPropertyValueChange}
                               value={inputs.phoneNumber} size="medium" className="w-100" InputProps={AppStyles.input}
                               InputLabelProps={AppStyles.input}/>
                </Col>
                <Col xs={12} md={5}>
                    <TextField id="webSite" type="text" label={t('web_site')}
                               onChange={onPropertyValueChange}
                               value={inputs.webSite} size="medium" className="w-100" InputProps={AppStyles.input}
                               InputLabelProps={AppStyles.input}/>
                </Col>
                <Col xs={12} md={3}>
                    <TextField id="email" type="text" label={t('email')}
                               onChange={onPropertyValueChange}
                               value={inputs.email} size="medium" className="w-100" InputProps={AppStyles.input}
                               InputLabelProps={AppStyles.input}/>
                </Col>
                <Col xs={12} md={1}/>
            </Row>
            <Row className="mt-4 mb-4">
                <Col xs={12} md={2}/>
                <Col xs={4} md={2}>
                    {office && (<>
                    <span style={{fontSize: 12, fontFamily: FONTS.Regular}}>{t('archived')}</span>
                    <Checkbox color="default" id="isArchived"  checked={inputs.isArchived} onChange={onPropertyValueChange} name="checkedG"/>
                    </>)}
                </Col>
                <Col xs={8} md={3}>
                    <Button className="w-100" onClick={() => {handleClick()}} style={AppStyles.validationButton}>
                        {ongoing ? <Spinner as="span" animation="grow" size="sm" role="status" aria-hidden="true"/> : null}
                        {office ? t('button.save_modification') : t('button.save_office')}
                    </Button>
                </Col>
                <Col xs={4} md={3}>
                    <Button variant="outline-dark" style={AppStyles.cancelButton}
                            onClick={() => {handleCancelClick()}}>{t('cancel')}</Button>
                </Col>
                <Col xs={12} md={2}/>
            </Row>
        </main>

    );
};

OfficeContent.propTypes = {
    customerId: PropTypes.string.isRequired,
    locations: PropTypes.array.isRequired,
    services: PropTypes.array,
    office: PropTypes.object,
};

OfficeContent.defaultProps = {
    customerId: "",
    locations: [],
    services: [],
};


export default OfficeContent;
