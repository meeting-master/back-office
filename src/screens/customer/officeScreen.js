import React, {useEffect, useState} from 'react';
import {useSelector} from "react-redux";
import {Accordion, Col, Row} from "react-bootstrap";


import {AddCircle, Edit, Visibility} from "@material-ui/icons";
//hooks
import useRematchDispatch from "../../hooks/useRematchDispatch";
import {useTranslation} from "react-i18next";

//components
import Colors from "../../utils/Colors";
import {AppStyles} from "../../css/styles";
import Page from "../../components/Page";

import OfficeContent from "./office";

const OfficeScreen = () => {
    const {t} = useTranslation();
    const {LoadLocations, GetOffices, GetServices} = useRematchDispatch(dispatch => ({
        LoadLocations: dispatch.location.LoadLocations,
        GetOffices: dispatch.customer.GetOffices,
        GetServices: dispatch.customer.GetServices,
    }));
    const offices = useSelector(state => state.customer.offices);
    const services = useSelector(state => state.customer.services);
    const locations = useSelector(state => state.location.locations);
    const customerId = useSelector(state => state.preference.customerId);

    const [selectedOfficeId, setSelectedOfficeId] = useState(null);

    useEffect(() => {
        if (customerId !== null) {
            GetServices(customerId);
            GetOffices(customerId);
        }
        if (locations.length === 0){
            LoadLocations();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleChangeClick = (id) => {
        setSelectedOfficeId(id === selectedOfficeId ? '' : id);
    };

    const getOfficeServices = serviceIds => {
        if (!serviceIds || serviceIds.length === 0) return;
        let value = "";
        services.forEach(service => {
            if (serviceIds.includes(service.id)) {
                value = value + ", " + service.name
            }
        });
        return value.slice(1);
    };

    const getDisplayValue = (name, districtId) => {
        let value = name;
        const loc = locations.find(loc => loc.id === districtId);
        if (loc) {
            value = name + " - " + loc.location
        }
        return value;
    };

    const renderOffice = (office) => {
        const iconStyle = selectedOfficeId === office.id ? AppStyles.ActiveIcon : AppStyles.normalIcon;
        const textStyle = selectedOfficeId === office.id ? AppStyles.modifyButtonText : AppStyles.greyBarText;

        return (
            <Row className="shadow mb-3" key={office.id}>
                <Col>
                    <Row className="align-items-center justify-content-center align-self-center"
                         style={AppStyles.accordionHeader}>
                        <Col xs={12} md={1}/>
                        <Col xs={4} md={4}>
                            {getDisplayValue(office.name, office.districtId)}
                        </Col>
                        <Col xs={3} md={3}>
                            {getOfficeServices(office.serviceIds)}
                        </Col>
                        <Col xs={2} md={1}>
                            {office.phoneNumber}
                        </Col>
                        <Col xs={1} md={1}>
                            {office.isArchived && t('archived')}
                        </Col>
                        <Col xs={2} md={2} className="d-flex flex-row-reverse">
                            <Accordion.Toggle eventKey={office.id}>
                                <div onClick={() => handleChangeClick(office.id)} style={AppStyles.modifyButton}>
                                    <Edit style={iconStyle}/>
                                    <span style={textStyle} className="ml-2"> {t('modify')}</span>
                                </div>
                            </Accordion.Toggle>
                        </Col>
                    </Row>
                    <Accordion.Collapse eventKey={office.id}>
                        {selectedOfficeId === office.id ? (
                            <OfficeContent customerId={customerId} services={services} locations={locations} office={office}/>
                        ) : <div/>}
                    </Accordion.Collapse>
                </Col>
            </Row>
        )
    };

    return (
        <Page title={'EasyMeeting ' + t(`menu_office`)}>
            <main className="container">
                <Row className="greyBar">
                    <Col className="d-flex align-items-center">
                        <AddCircle style={{color: Colors.black}}/> <span style={AppStyles.greyBarText}
                                                                         className="ml-2"> {t('labels.create_office')}</span>
                    </Col>
                </Row>
                <OfficeContent customerId={customerId} services={services} locations={locations}/>
                <Row className="greyBar">
                    <Col className="d-flex align-items-center">
                        <Visibility style={{color: Colors.black}}/> <span style={AppStyles.greyBarText}
                                                                          className="ml-2"> {t('labels.view_offices')}</span>
                    </Col>
                </Row>
                <Accordion>
                    {offices.map(s => renderOffice(s))}
                </Accordion>
            </main>
        </Page>
    );
};

export default OfficeScreen;
