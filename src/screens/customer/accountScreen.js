import React, {useEffect, useState} from 'react';
import {useSelector} from "react-redux";
import {Button, Col, Row, Spinner} from "react-bootstrap";
import {X} from "react-feather";
import {Edit} from "@material-ui/icons";
import {TextField} from "@material-ui/core";

//services
import {useTranslation} from "react-i18next";
import useRematchDispatch from "../../hooks/useRematchDispatch";
import Colors from "../../utils/Colors";
import {AppStyles} from "../../css/styles";

//components
import Page from "../../components/Page";


const AccountScreen = () => {
    const {t} = useTranslation();
    const {GetAccount, UpdateAccount} = useRematchDispatch(dispatch => ({
        UpdateAccount: dispatch.customer.UpdateAccount,
        GetAccount: dispatch.customer.GetAccount,
    }));
    const ongoing = useSelector(state => state.customer.ongoing);
    const currentCustomer = useSelector(state => state.customer.currentCustomer);
    const customerId = useSelector(state => state.preference.customerId);
    const [inputs, setInputs] = useState({
        name: "",
        activities: "",
        phoneNumber: "",
        webSite: "",
        tags: [],
    });
    const [errors, setErrors] = useState({
        name: false,
        activities: false,
    });

    const [logoFile, setLogoFile] = useState();
    const [logoPreview, setLogoPreview] = useState();
    const [tag, setTag] = useState();
    const maxSize = 250;
    const img = document.createElement('img');

    useEffect(() => {
        if (currentCustomer === null) {
            GetAccount(customerId);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        setInputs({...inputs, ...currentCustomer});
        setLogoPreview(currentCustomer.logoUrl);
        // eslint-disable-next-line
    }, [currentCustomer]);

    const onPropertyValueChange = (event) => {
        let value = event.target.value;
        setInputs({...inputs, [event.target.id]: value});
    };

    const handleClick = () => {
        if (inputs.name.length === 0) {
            setErrors(errs => ({...errs, "name": true}));
            return;
        } else {
            setErrors(errs => ({...errs, "name": false}));
        }
        if (inputs.activities.length === 0) {
            setErrors(errs => ({...errs, "activities": true}));
            return;
        } else {
            setErrors(errs => ({...errs, "activities": false}));
        }
        inputs.customerId = localStorage.getItem('CustomerId');
        const formData = new FormData();
        if (logoFile) {
            formData.append('logo', logoFile);
        }
        formData.append('data', JSON.stringify(inputs));
        UpdateAccount({customerId, formData});
        URL.revokeObjectURL(logoPreview);
    };

    const handleTagAdd = () => {
        if (tag && tag.trim().length > 0) {
            let tags = inputs.tags;
            tags.push(tag);
            setInputs({...inputs, "tags": tags});
            setTag('');
        }
    };

    const handleCancelTag = (index) => {
        let tags = inputs.tags;
        tags.splice(index, 1);
        setInputs({...inputs, "tags": tags});
    };

    const handleImageChoose = (event) => {
        img.src = URL.createObjectURL(event.target.files[0]);
    };

    const handleCancelClick = () => {
        setInputs(currentCustomer);
        setLogoPreview(currentCustomer.logoUrl);
    };

    img.onload = () => {
        const canvas = document.createElement('canvas');
        let width = img.width;
        let height = img.height;
        if (width > height) {
            if (width > maxSize) {
                height *= maxSize / width;
                width = maxSize;
            }
        } else {
            if (height > maxSize) {
                width *= maxSize / height;
                height = maxSize;
            }
        }
        canvas.width = width;
        canvas.height = height;
        canvas.getContext('2d').drawImage(img, 0, 0, width, height);
        canvas.toBlob((blob) => {
            const file = new File([blob], new Date().getTime() + ".jpeg", {
                type: 'image/jpeg',
                lastModified: Date.now()
            });
            setLogoFile(file);
        }, 'image/jpeg', 0.8);
        setLogoPreview(canvas.toDataURL('image/jpeg', 0.8));
    };

    const renderTags = () => {
        return (
            <ul id="tag-list">
                {inputs.tags.map((tag, i) =>{
                return tag.trim().length > 0 && <li key={i} id="tag-list">
                        <span style={AppStyles.greyBarText} className="mr-1">{tag}</span>
                        <X size={14} onClick={() => {
                            handleCancelTag(i)
                        }} style={{cursor: 'pointer', color: Colors.grey}}/>
                    </li>})}
            </ul>

        )
    };

    return (
        <Page title={'EasyMeeting ' + t(`menu_settings`)}>
            <main className="container">
                <Row className="greyBar">
                    <Col className="d-flex align-items-center">
                        <Edit style={{color: Colors.black}} size={12}/> <span style={AppStyles.greyBarText}
                                                                              className="ml-2"> {t('labels.menu_settings')}</span>
                    </Col>
                </Row>
                <Row className="text-center mt-2">
                    <Col xs={12} md={2}/>
                    <Col xs={12} md={4}>
                        <TextField id="name" type="text" placeholder={t('name')} className="w-100" label={t('name')}
                                   error={errors.name} value={inputs.name}
                                   size="medium" InputProps={AppStyles.input} InputLabelProps={AppStyles.input}
                                   onChange={onPropertyValueChange}/>
                    </Col>

                    <Col xs={12} md={4}>
                        <TextField id="activities" type="text" placeholder={t('customer_activities')}
                                   label={t('phone_number')} className="w-100"
                                   error={errors.activities} value={inputs.activities}
                                   size="medium" InputProps={AppStyles.input} InputLabelProps={AppStyles.input}
                                   onChange={onPropertyValueChange}/>
                    </Col>
                    <Col xs={12} md={2}/>
                </Row>
                <Row className="text-center mt-2">
                    <Col xs={12} md={2}/>
                    <Col xs={12} md={4}>
                        <TextField id="phoneNumber" type="text" placeholder={t('phone_number')}
                                   label={t('phone_number')} className="w-100"
                                   error={errors.phoneNumber} value={inputs.phoneNumber}
                                   size="medium" InputProps={AppStyles.input} InputLabelProps={AppStyles.input}
                                   onChange={onPropertyValueChange}/>
                    </Col>
                    <Col xs={12} md={4}>
                        <TextField id="webSite" type="url" placeholder={t('web_site')} label={t('web_site')}
                                   className="w-100"
                                   value={inputs.webSite}
                                   size="medium" InputProps={AppStyles.input} InputLabelProps={AppStyles.input}
                                   onChange={onPropertyValueChange}/>
                    </Col>
                    <Col xs={12} md={2}/>
                </Row>
                <Row className="mt-2">
                    <Col xs={12} md={2}/>
                    <Col xs={8} md={3}>
                        <TextField id="search_tag" type="text" placeholder={t('search_tag_holder')} className="w-100"
                                   label={t('search_tag')} value={tag}
                                   size="medium" inputProps={styles.tag} InputLabelProps={AppStyles.input}
                                   onChange={event => setTag(event.target.value)}/>
                    </Col>

                    <Col xs={4} md={1}>
                        <Button className="mt-1" style={AppStyles.cancelButton} variant="outline-dark"
                                disabled={inputs.tags.length >= 5} onClick={() => {
                            handleTagAdd()
                        }}>{t('add')}</Button>
                    </Col>
                    <Col xs={12} md={6} className="mt-2">
                        {renderTags()}
                    </Col>

                </Row>
                <Row className="mt-2">
                    <Col xs={12} md={2}/>
                    <Col xs={8} md={3}>
                        <TextField type="file" className="w-100" label={t('logo')}
                                   accept="image/*"
                                   size="medium" InputProps={AppStyles.input} InputLabelProps={AppStyles.input}
                                   onChange={handleImageChoose}
                        />
                    </Col>
                    <Col xs={12} md={6}> {logoPreview && <img src={logoPreview} alt="logo"/>}</Col>
                </Row>
                <Row className="mt-4 mb-4">
                    <Col xs={12} md={2}/>
                    <Col xs={8} md={3}>
                        <Button className="w-100" onClick={() => {
                            handleClick()
                        }} style={AppStyles.validationButton}>
                            {ongoing ?
                                <Spinner as="span" animation="grow" size="sm" role="status" aria-hidden="true"/> : null}
                            {t('button.save_modification')}
                        </Button>
                    </Col>
                    <Col xs={4} md={3}>
                        <Button variant="outline-dark" style={AppStyles.cancelButton} onClick={() => {handleCancelClick()}}>{t('cancel')}</Button>
                    </Col>
                    <Col xs={12} md={4}/>
                </Row>
            </main>
        </Page>
    );
};

const styles = {
  tag: {
      style: {fontSize: 11},
      maxlength: 20
  }
};
export default AccountScreen
