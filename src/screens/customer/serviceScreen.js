import React, {useEffect, useState} from 'react';
import {useSelector} from "react-redux";
import {Accordion, Button, Col, Row, Spinner} from "react-bootstrap";
import {AddCircle, Edit, Visibility} from "@material-ui/icons";
import TextField from "@material-ui/core/TextField";

//hooks
import useRematchDispatch from "../../hooks/useRematchDispatch";

//services
import Colors from "../../utils/Colors";
import {useTranslation} from "react-i18next";
import Page from "../../components/Page";
import {AppStyles} from "../../css/styles";
import FONTS from "../../utils/fonts";
import {Checkbox} from "@material-ui/core";


const ServiceScreen = () => {
    const {t} = useTranslation();
    const {UpSertService, GetServices} = useRematchDispatch(dispatch => ({
        UpSertService: dispatch.customer.UpSertService,
        GetServices: dispatch.customer.GetServices,
    }));
    const ongoing = useSelector(state => state.customer.ongoing);
    const services = useSelector(state => state.customer.services);
    const customerId = useSelector(state => state.preference.customerId);
    const [name, setName] = useState("");
    const [alias, setAlias] = useState("");
    const [activeName, setActiveName] = useState("");
    const [activeAlias, setActiveAlias] = useState("");
    const [isArchived, setIsArchived] = useState(false);

    const [errors, setErrors] = useState({
        name: false,
    });
    const [selectedServiceId, setSelectedServiceId] = useState();

    useEffect(() => {
        if (customerId !== null) {
            GetServices(customerId);
        }
        // eslint-disable-next-line
    }, []);

    useEffect(() => {
        const selectedService = services.find(x => x.id === selectedServiceId);
        let n = "";
        let a = "";
        let s = false;
        if (selectedService !== undefined) {
            n = selectedService.name;
            a = selectedService.alias;
            s = selectedService.isArchived;
        }
        setActiveName(n);
        setActiveAlias(a);
        setIsArchived(s);
        // eslint-disable-next-line
    }, [selectedServiceId]);


    const handleClick = () => {
        if (selectedServiceId) {
            if (activeName.length === 0) {
                setErrors(errs => ({...errs, "activeName": true}));
                return;
            } else {
                setErrors(errs => ({...errs, "activeName": false}));
            }
        } else {

            if (name.length === 0) {
                setErrors(errs => ({...errs, "name": true}));
                return;
            } else {
                setErrors(errs => ({...errs, "name": false}));
            }
        }
        const n = selectedServiceId ? activeName : name;
        const a = selectedServiceId ? activeAlias : alias;
        UpSertService({name: n, alias: a, customerId, isArchived, id: selectedServiceId});
    };

    const handleNewClick = () => {
        setSelectedServiceId("");
        setErrors("");
    };
    const handleChangeClick = (id) => {
        setSelectedServiceId(id === selectedServiceId ? '' : id);
    };

    const handleCheck = (e) => {
        setIsArchived(e.target.checked);
    };

    const renderServices = (service) => {
        const iconStyle = selectedServiceId === service.id ? AppStyles.ActiveIcon : AppStyles.normalIcon;
        const textStyle = selectedServiceId === service.id ? AppStyles.modifyButtonText : AppStyles.greyBarText;
        return (
            <Row className="shadow mb-3" key={service.id}>
                <Col>
                    <Row className="align-items-center justify-content-center align-self-center"
                         style={AppStyles.accordionHeader}>
                        <Col xs={12} md={2}/>
                        <Col xs={6} md={3}>
                            {service.name}
                        </Col>
                        <Col xs={3} md={1}>
                            {service.alias}
                        </Col>
                        <Col xs={1} md={1}>
                            {service.isArchived && t('archived')}
                        </Col>
                        <Col xs={2} md={4} className="d-flex flex-row-reverse">
                            <Accordion.Toggle eventKey={service.id}>
                                <div onClick={() => handleChangeClick(service.id)} style={AppStyles.modifyButton}>
                                    <Edit style={iconStyle}/>
                                    <span style={textStyle} className="ml-2"> {t('modify')}</span>
                                </div>
                            </Accordion.Toggle>
                        </Col>
                    </Row>
                    <Accordion.Collapse eventKey={service.id}>
                        <Row className="editZone">
                            <Col>
                                <Row className="text-center">
                                    <Col xs={12} md={2}/>
                                    <Col xs={12} md={3}>
                                        <TextField id={"name" + service.id} type="text" label={t('name')}
                                                   error={errors.activeName}
                                                   onChange={(event) => setActiveName(event.target.value)}
                                                   InputProps={AppStyles.input} InputLabelProps={AppStyles.input}
                                                   value={activeName} size="medium" className="w-100"/>
                                    </Col>
                                    <Col xs={12} md={3}>
                                        <TextField id={"alias" + service.id} type="text" label={t('alias')}
                                                   onChange={(event) => setActiveAlias(event.target.value)}
                                                   InputProps={AppStyles.input} InputLabelProps={AppStyles.input}
                                                   value={activeAlias} size="medium" className="w-100"/>
                                    </Col>
                                    <Col xs={12} md={2}>
                                        <span style={{fontSize: 12, fontFamily: FONTS.Regular}}>{t('archived')}</span>
                                        <Checkbox color="default" id="isArchived" checked={isArchived}
                                                  onChange={(event) => {
                                                      handleCheck(event)
                                                  }}/>
                                    </Col>
                                    <Col xs={12} md={4}/>
                                </Row>
                                <Row className="mt-4 mb-4">
                                    <Col xs={12} md={2}/>
                                    <Col xs={8} md={3}>
                                        <Button className="w-100" onClick={() => {
                                            handleClick()
                                        }} style={AppStyles.validationButton}>
                                            {ongoing ?
                                                <Spinner as="span" animation="grow" size="sm" role="status"
                                                         aria-hidden="true"/> : null}{t('button.save_modification')}</Button>
                                    </Col>
                                    <Col xs={4} md={3}>
                                        <Accordion.Toggle eventKey={service.id}>
                                            <div style={AppStyles.cancelButton} className="cancelButton"
                                                 onClick={() => {
                                                     handleNewClick()
                                                 }}>{t('cancel')}</div>
                                        </Accordion.Toggle>
                                    </Col>
                                    <Col xs={12} md={4}/>
                                </Row>
                            </Col>
                        </Row>
                    </Accordion.Collapse>
                </Col>
            </Row>
        )
    };
    return (
        <Page title={'EasyMeeting ' + t(`menu_service`)}>
            <main className="container">
                <Row className="greyBar">
                    <Col className="d-flex align-items-center">
                        <AddCircle style={{color: Colors.black}}/> <span style={AppStyles.greyBarText}
                                                                         className="ml-2"> {t('labels.create_service')}</span>
                    </Col>
                </Row>
                <Row className="text-center">
                    <Col xs={12} md={2}/>
                    <Col xs={12} md={3}>
                        <TextField id="name" type="text" label={t('name')} error={errors.name} required
                                   onChange={(event) => setName(event.target.value)}
                                   InputProps={AppStyles.input} InputLabelProps={AppStyles.input}
                                   value={name} size="medium" className="w-100"/>
                    </Col>
                    <Col xs={12} md={3}>
                        <TextField id="alias" type="text" label={t('alias')}
                                   onChange={(event) => setAlias(event.target.value)}
                                   InputProps={AppStyles.input} InputLabelProps={AppStyles.input}
                                   value={alias} size="medium" className="w-100"/>
                    </Col>
                    <Col xs={12} md={4}/>
                </Row>
                <Row className="mt-4 mb-4">
                    <Col xs={12} md={2}/>
                    <Col xs={8} md={3}>
                        <Button className="w-100" onClick={() => {
                            handleClick()
                        }} style={AppStyles.validationButton}>
                            {ongoing ?
                                <Spinner as="span" animation="grow" size="sm" role="status"
                                         aria-hidden="true"/> : null}{t('button.save_service')}</Button>
                    </Col>
                    <Col xs={4} md={3}>
                        <Button variant="outline-dark" style={AppStyles.cancelButton}
                                onClick={() => {
                                    handleNewClick()
                                }}>{t('cancel')}</Button>
                    </Col>
                    <Col xs={12} md={4}/>
                </Row>
                <Row className="greyBar">
                    <Col className="d-flex align-items-center">
                        <Visibility style={{color: Colors.black}}/> <span style={AppStyles.greyBarText}
                                                                          className="ml-2"> {t('labels.view_services')}</span>
                    </Col>
                </Row>
                <Accordion>
                    {services.map(s => renderServices(s))}
                </Accordion>
            </main>
        </Page>
    );
};

export default ServiceScreen;
