import React, {useEffect, useState} from 'react';
import {useSelector} from "react-redux";
import {Accordion, Card, Col, Row, ToggleButton, ToggleButtonGroup} from "react-bootstrap";
import {BlurLinear, Business, Visibility} from "@material-ui/icons";
import {CheckCircle, ChevronDown, ChevronUp, Clock, Trash2} from 'react-feather';
import {
    endOfDay,
    endOfMonth,
    endOfWeek,
    formatRFC3339,
    startOfDay,
    startOfMonth,
    startOfWeek,
    subDays,
    subMonths
} from 'date-fns'
//hooks
import {useTranslation} from "react-i18next";
import useRematchDispatch from "../hooks/useRematchDispatch";
//services
import Colors from "../utils/Colors";
import FONTS from "../utils/fonts";
import Page from "../components/Page";

import {AppStyles} from "../css/styles";


const Period = {
    Week: "W",
    Month: "M",
    Quarter: "Q"
};

const HomeScreen = () => {
    const {t} = useTranslation();
    const {
        LoadStatistics,
        GetOffices,
        //LoadOfficeStatistics,
        GetServices,
        LoadLocations,
        GetCalendarTimeSlots,
        GetTimeSlotAppointments
    } = useRematchDispatch(dispatch => ({
        LoadWeekData: dispatch.statistic.LoadWeekAppointmentData,
        LoadStatistics: dispatch.statistic.LoadStatistics,
        //LoadOfficeStatistics: dispatch.statistic.LoadOfficeStatistics,
        GetOffices: dispatch.customer.GetOffices,
        GetServices: dispatch.customer.GetServices,
        GetCalendarTimeSlots: dispatch.calendar.GetCalendarTimeSlots,
        LoadLocations: dispatch.location.LoadLocations,
        GetTimeSlotAppointments: dispatch.calendar.GetTimeSlotAppointments,
    }));
    //const graphData = useSelector(state => state.statistic.graphData);
    //const statData = useSelector(state => state.statistic.statData);
    const customerId = useSelector(state => state.preference.customerId);
    const offices = useSelector(state => state.customer.offices);
    const services = useSelector(state => state.customer.services);
    const locations = useSelector(state => state.location.locations);
    const calendarTimeSlotByHours = useSelector(state => state.calendar.calendarTimeSlotByHours);
    const timeSlotAppointments = useSelector(state => state.calendar.timeSlotAppointments);
    const timeSlotByIds = useSelector(state => state.calendar.timeSlotByIds);
    const appointmentCountByOfficeId = useSelector(state => state.statistic.appointmentCountByOfficeId);
    const canceledCountByOfficeId = useSelector(state => state.statistic.canceledCountByOfficeId);
    const consumedCountByOfficeId = useSelector(state => state.statistic.consumedCountByOfficeId);
    const [termValue, setTermValue] = useState(null);
    // eslint-disable-next-line
    const [selectedOffice, setSelectedOffice] = useState(null);
    const [displayHour, setDisplayHour] = useState("");
    const [displayDay, setDisplayDay] = useState("");

    const [selectedOfficeId, setSelectedOfficeId] = useState(null);
    const [selectedServiceId, setSelectedServiceId] = useState(null);

    useEffect(() => {
        setTermValue(Period.Week);
        // eslint-disable-next-line
    }, []);

    useEffect(() => {
        if (customerId !== null) {
            GetServices(customerId);
            GetOffices(customerId);
        }
        if (locations.length === 0) {
            LoadLocations();
        }
        // eslint-disable-next-line
    }, [customerId]);

    useEffect(() => {
        handlePeriodChange();
        if (offices.length === 0 && customerId !== null) {
            GetOffices(customerId);
        }
        // eslint-disable-next-line
    }, [termValue, customerId]);

    /*useEffect(() => {
        if (selectedOffice !== null) {
            LoadOfficeStatistics({id: selectedOffice, format: t('graph_date_format')});
        }
    }, [selectedOffice]);*/

    const handlePeriodChange = () => {
        if (termValue === null || customerId === null) {
            return;
        }
        let term;
        switch (termValue) {
            case Period.Week:
                term = week();
                break;
            case Period.Month:
                term = month();
                break;
            case Period.Quarter:
                term = quarter();
                break;
            default:
                term = week();
        }

        LoadStatistics({
            customerId,
            startDate: formatRFC3339(term.startDate),
            endDate: formatRFC3339(term.endDate)
        });
        setSelectedOffice(null);
    };

    const week = () => {
        const startDate = startOfDay(startOfWeek(new Date()));
        const endDate = endOfDay(endOfWeek(new Date()));
        return {startDate, endDate};
    };

    const month = () => {
        const startDate = startOfDay(startOfMonth(new Date()));
        const endDate = endOfDay(endOfMonth(new Date()));
        return {startDate, endDate};
    };

    const quarter = () => {
        const endDate = endOfMonth(subDays(startOfMonth(new Date()), 1));
        const startDate = startOfMonth(subMonths(endDate, 2));
        return {startDate, endDate};
    };

    // const renderGraph = () => {
    //     return (
    //         <main className="card">
    //             <div className="card-header" style={{fontFamily: FONTS.Bold}}>
    //                 <FontAwesomeIcon icon={faChartLine} color={Colors.grey} size="lg"/>
    //             </div>
    //             <div className="text-center">
    //                 {graphData === null || graphData.dataStreams.length === 0 ?
    //                     <FontAwesomeIcon icon={faChartLine} color={Colors.greyhish} size="10x"/> :
    //                     <LinearGraph data={graphData?.dataStreams} legend={graphData?.legend}/>}
    //             </div>
    //         </main>
    //     )
    // };


    // const renderList = () => {
    //     return (
    //         <main className="card mr-0">
    //             <div className="card-header" style={{fontFamily: FONTS.Regular}}>
    //                 {t('office_list')}
    //             </div>
    //             <div className="card-body">
    //                 {offices.map(x => {
    //                         const bgColor = x.id === selectedOffice ? Colors.greyhish : Colors.white;
    //                         return (
    //                             <div className="shadow  p-3 mb-2 rounded"
    //                                  style={{cursor: 'pointer', backgroundColor: bgColor}} key={x.name}
    //                                  onClick={() => setSelectedOffice(x.id)}>
    //                                 <span style={{fontFamily: FONTS.Italic}}>{x.name}</span> <span
    //                                 className="float-right font-weight-bold">{x.value}</span>
    //                             </div>)
    //                     }
    //                 )}
    //             </div>
    //         </main>
    //     )
    // };

    const renderX = (office) => {
        return (
            <Card>
                <Card.Header style={AppStyles.cardH}>
                    {office.name}
                </Card.Header>
                <Card.Body>
                    <div className="d-flex align-items-center justify-content-between w-100">
                        <div style={AppStyles.homeCalendarContentText}>
                            <Clock size="20" color={Colors.royalblue}/>
                            <span className="ml-2">{t("appointments")}</span>
                        </div>
                        <div className="d-flex  align-items-center" style={AppStyles.homeCalendarContentText}>
                            {appointmentCountByOfficeId[office.id] ? appointmentCountByOfficeId[office.id] : 0}
                        </div>
                    </div>
                    <div className="ad"/>
                    <div className="d-flex align-items-center justify-content-between w-100">
                        <div style={AppStyles.homeCalendarContentText}>
                            <Trash2 size="20" color='red'/>
                            <span className="ml-2">{t('cancellations')}</span>
                        </div>
                        <div className="d-flex  align-items-center" style={AppStyles.homeCalendarContentText}>
                            {canceledCountByOfficeId[office.id] ? canceledCountByOfficeId[office.id] : 0}
                        </div>
                    </div>
                    <div className="ad"/>
                    <div className="d-flex align-items-center justify-content-between w-100">
                        <div style={AppStyles.homeCalendarContentText}>
                            <CheckCircle size="20" color={Colors.darkgreen}/>
                            <span className="ml-2">{t('executed')}</span>
                        </div>
                        <div className="d-flex  align-items-center" style={AppStyles.homeCalendarContentText}>
                            {consumedCountByOfficeId[office.id] ? consumedCountByOfficeId[office.id] : 0}
                        </div>
                    </div>
                </Card.Body>
            </Card>
        )
    };

    const getServiceName = id => {
        const service = services.find(x => x.id === id);
        return service ? service.name : "";
    };

    const handleChangeClick = (officeId, serviceId) => {
        if (selectedOfficeId === null || selectedOfficeId !== officeId) {
            setSelectedOfficeId(officeId);
        } else if (selectedOfficeId === officeId) {
            setSelectedOfficeId(null);
        }

        if (selectedServiceId === null || selectedServiceId !== serviceId) {
            setSelectedServiceId(serviceId);
        } else if (selectedServiceId === serviceId) {
            setSelectedServiceId(null);
        }
        loadCalendarTimeSlots(officeId, serviceId);
    };

    const loadCalendarTimeSlots = (officeId, serviceId) => {
        const startDate = startOfDay(startOfWeek(new Date()));
        const endDate = endOfDay(endOfWeek(new Date()));
        GetCalendarTimeSlots({
            officeId,
            serviceId,
            startDate,
            endDate
        });
    };

    const handleAppointmentClick = (hourRange, day, calendarTimeSlotIds) => {
        setDisplayHour(hourRange);
        switch (day) {
            case "0":
                setDisplayDay(t("sunday"));
                break;
            case "1":
                setDisplayDay(t("monday"));
                break;
            case "2":
                setDisplayDay(t("tuesday"));
                break;
            case "3":
                setDisplayDay(t("wednesday"));
                break;
            case "4":
                setDisplayDay(t("thursday"));
                break;
            case "5":
                setDisplayDay(t("friday"));
                break;
            case "6":
                setDisplayDay(t("saturday"));
                break;
            default:
                setDisplayDay("");
        }
        GetTimeSlotAppointments(calendarTimeSlotIds);
    };

    const renderCalendar = () => {
        return (
            <Row className="align-self-center">
                <Col xs={12} md={6}>
                    <span style={AppStyles.homeCalendarHeaderText}>{t('week_appointment')}</span>
                    <table className="shadow rounded mt-3">
                        <thead>
                        <tr>
                            <th className="border border-secondary"
                                style={AppStyles.homeCalendarTableHeaderBlack}>{t('menu_schedule')}</th>
                            <th className="border border-secondary"
                                style={AppStyles.homeCalendarTableHeaderBlue}>{t('sunday')}</th>
                            <th className="border border-secondary"
                                style={AppStyles.homeCalendarTableHeaderBlue}>{t('monday')}</th>
                            <th className="border border-secondary"
                                style={AppStyles.homeCalendarTableHeaderBlue}>{t('tuesday')}</th>
                            <th className="border border-secondary"
                                style={AppStyles.homeCalendarTableHeaderBlue}>{t('wednesday')}</th>
                            <th className="border border-secondary"
                                style={AppStyles.homeCalendarTableHeaderBlue}>{t('thursday')}</th>
                            <th className="border border-secondary"
                                style={AppStyles.homeCalendarTableHeaderBlue}>{t('friday')}</th>
                            <th className="border border-secondary"
                                style={AppStyles.homeCalendarTableHeaderBlue}>{t('saturday')}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            Object.keys(calendarTimeSlotByHours).map((key, index) => {
                                let calendarTimeSlotByHour = calendarTimeSlotByHours[key][0];
                                let hourRange = `${key}h00 - ${parseInt(key, 10) + 1}h00`;
                                return (
                                    <tr key={index}>
                                        <td className="border" style={AppStyles.homeCalendarTableBodyGrey}>
                                            {hourRange}
                                        </td>
                                        {Object.keys(calendarTimeSlotByHour).map((day, timeSlotIndex) => {
                                            return (
                                                <td key={timeSlotIndex} className="border"
                                                    style={AppStyles.homeCalendarTableBody}>
                                                    {calendarTimeSlotByHour[day].length > 0 ?
                                                        <Clock style={{cursor: 'pointer'}} size="20"
                                                               color={Colors.royalblue}
                                                               onClick={() => handleAppointmentClick(hourRange, day, calendarTimeSlotByHour[day])}/>
                                                        : ""}
                                                </td>
                                            )
                                        })}

                                    </tr>
                                )
                            })
                        }
                        </tbody>
                    </table>
                </Col>
                {timeSlotAppointments.length > 0 ? <Col xs={12} md={4}>
                        <span style={AppStyles.homeCalendarHeaderText}>&nbsp;</span>
                        <Card className="shadow mt-3">
                            <Card.Header style={AppStyles.homeCalendarTableHeaderBlack}>
                                {`${displayDay} ${displayHour}`}
                            </Card.Header>
                            <Card.Body>
                                {timeSlotAppointments.map((timeSlotAppointment, index) => {
                                    const userDetails = JSON.parse(timeSlotAppointment.details);
                                    const startTime = timeSlotByIds[timeSlotAppointment.timeSlotId][0].startTime;
                                    return (
                                        <React.Fragment>
                                            <div className="d-flex align-items-center justify-content-between w-100">
                                                <div style={styles.line}>
                                                    <Clock size="20" color={Colors.royalblue} className="mr-2"
                                                           style={AppStyles.homeCalendarHeaderText}/>
                                                    <span style={AppStyles.homeCalendarContentText}>{startTime}</span>
                                                </div>
                                            </div>
                                            <div className="d-flex align-items-center justify-content-between w-100">
                                                <div className="d-flex  align-items-center pl-1">
                                                    <span className="ml-4"
                                                          style={AppStyles.homeCalendarContentText}>{`${userDetails.firstName} ${userDetails.lastName}`}</span>
                                                </div>
                                            </div>
                                            <div className="d-flex align-items-center justify-content-between w-100">
                                                <div className="d-flex  align-items-center pl-1">
                                                    <span className="ml-4"
                                                          style={AppStyles.homeCalendarContentText}>{`${userDetails.phoneNumber}`}</span>
                                                </div>
                                            </div>
                                            <div className="d-flex align-items-center justify-content-between w-100">
                                                <div className="d-flex  align-items-center pl-1">
                                                    <span className="ml-4"
                                                          style={AppStyles.homeCalendarContentText}>{`${userDetails.email}`}</span>
                                                </div>
                                            </div>
                                            <div className="ad"/>
                                        </React.Fragment>
                                    )
                                })}
                            </Card.Body>
                        </Card>
                    </Col>
                    : <Col xs={12} md={4}/>}
            </Row>
        )
    };

    const renderOfficeService = (officeId, officeName, serviceId) => {

        const key = officeId + serviceId;

        return (
            <Row className="shadow mb-2" key={key}>
                <Col>
                    <Row className="align-items-center justify-content-between align-self-center"
                         style={AppStyles.homeAccordionHeader}>
                        <Col xs={10} md={10}>
                            <Business fontSize="small"
                                      className="mr-2"/>{`${officeName} - ${getServiceName(serviceId)}`}
                        </Col>
                        <Col xs={2} md={2} className="d-flex flex-row-reverse">
                            <Accordion.Toggle eventKey={key}>
                                <div onClick={() => handleChangeClick(officeId, serviceId)}
                                     style={AppStyles.modifyButton}>
                                    {selectedOfficeId === officeId && selectedServiceId === serviceId ?
                                        <ChevronUp style={AppStyles.ActiveIcon} size={15}/> : <ChevronDown size={15}/>}
                                </div>
                            </Accordion.Toggle>
                        </Col>
                    </Row>
                    <Accordion.Collapse eventKey={key}>
                        {selectedOfficeId === officeId && selectedServiceId === serviceId ? (
                            <div className="pt-3 pb-3">{renderCalendar()}</div>
                        ) : <div/>}
                    </Accordion.Collapse>
                </Col>
            </Row>

        )
    };

    const getDisplayValue = (name, districtId) => {
        let value = name;
        const loc = locations.find(loc => loc.id === districtId);
        if (loc) {
            value = name + " - " + loc.location
        }
        return value;
    };

    const renderOffice = (office) => {
        const officeName = getDisplayValue(office.name, office.districtId);
        return (
            <>
                {office.serviceIds.map(id => {
                    return renderOfficeService(office.id, officeName, id)
                })}
            </>
        )
    };

    return (
        <Page title={'EasyMeeting ' + t(`labels.menu_dashboard`)}>
            <div className="container">
                <Row className="greyBar">
                    <Col className="d-flex align-items-center">
                        <BlurLinear style={{color: Colors.black}}/>
                        <span style={AppStyles.greyBarText} className="ml-2"> {t('labels.menu_dashboard')}</span>
                    </Col>
                </Row>
                <Row className="m-2">
                    <Col xs={12} sm={8}/>
                    <Col xs={12} sm={4}>
                        <ToggleButtonGroup type="radio" value={termValue} onChange={(val) => setTermValue(val)}
                                           name="toggleButton">
                            <ToggleButton style={AppStyles.homeCalendarButtonGroupText} value={Period.Week}
                                          variant="outline-dark">{t('current_week')}</ToggleButton>
                            <ToggleButton style={AppStyles.homeCalendarButtonGroupText} value={Period.Month}
                                          variant="outline-dark">{t('current_month')}</ToggleButton>
                            <ToggleButton style={AppStyles.homeCalendarButtonGroupText} value={Period.Quarter}
                                          variant="outline-dark">{t('last_three_months')}</ToggleButton>
                        </ToggleButtonGroup>
                    </Col>
                </Row>

                <Row className="d-flex">
                    {offices.map(x => {
                        return (
                            <Col xs={12} md={6} className="mb-2" key={x.id}>
                                {renderX(x)}
                            </Col>)
                    })}

                </Row>
                <Row className="greyBar">
                    <Col className="d-flex align-items-center">
                        <Visibility style={{color: Colors.black}}/>
                        <span style={AppStyles.greyBarText} className="ml-2"> {t('week_appointment')}</span>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Accordion>
                            {offices.filter(x => x.isArchived !== true).map(office => renderOffice(office))}
                        </Accordion>
                    </Col>
                </Row>
            </div>
        </Page>
    );
};

const styles = {
    cardH: {
        backgroundColor: '#373737',
        color: Colors.white,
        fontFamily: FONTS.SemiBold,
        paddingLeft: 30,
    },
    line: {
        fontSize: 16,
        fontFamily: FONTS.SemiBold,
        color: '#373737',
    },
    number: {
        fontSize: 20,
    },
    bar: {background: `linear-gradient(190deg, ${Colors.lightblue}, ${Colors.royalblue})`},
    name: {
        color: Colors.white,
        fontFamily: FONTS.Regular,
    },
    logOut: {
        marginLeft: -12,
    }
};

export default HomeScreen;
