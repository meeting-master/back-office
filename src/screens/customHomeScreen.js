import React, { useEffect } from 'react';
import { useSelector } from "react-redux";

import LinearGraph from "../components/linearGraph";
import { Col, Row } from "react-bootstrap";
import { faFemale, faMale, faUserFriends } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

//hooks
import { useTranslation } from "react-i18next";
import useRematchDispatch from "../hooks/useRematchDispatch";

//services
import Colors from "../utils/Colors";
import FONTS from "../utils/fonts";

const CHomeScreen = () => {
    const {t} = useTranslation();
    const {LoadWeekData, LoadOfficeSummaries} = useRematchDispatch(dispatch => ({
        LoadWeekData: dispatch.graph.LoadWeekAppointmentData,
        LoadOfficeSummaries: dispatch.graph.LoadOfficeSummaries,
    }));
    const weekData = useSelector(state => state.graph.graphData);
    const officeSummaries = useSelector(state => state.graph.officeSummaries);

    useEffect(() => {
        LoadWeekData();
        LoadOfficeSummaries();
    }, []);

    const renderGraph = () => {
        return (
            <main className="card">
                <div className="card-header" style={{fontFamily: FONTS.Bold}}>
                    {t('Statistiques')}
                </div>
                <div className="text-center">
                    {weekData !== null && <LinearGraph data={weekData.dataStreams} legend={weekData.legend}/>}
                </div>
            </main>
        )
    }

    const renderList = () => {
        return (
            <main className="card mr-0">
                <div className="card-header" style={{fontFamily: FONTS.Bold}}>
                    {t('Centres')}
                </div>
                <div className="card-body">
                    {officeSummaries.map(x =>
                        <div className="shadow  p-3 mb-2 bg-white rounded" key={x.name}>
                            <span style={{fontFamily: FONTS.Italic}}>{x.name}</span> <span
                            className="float-right font-weight-bold">{x.value}</span>
                        </div>
                    )}
                </div>
            </main>
        )
    }

    return (
        <main >
            <div>
                <span style={{fontFamily: FONTS.ExtraBold, margin: 10}}>APERCU</span>
                <div className="d-flex justify-content-between" style={{marginTop: 20}}>
                            <div className="shadow p-3 mb-5 rounded bg-white" style={{width: '30%'}}>
                                <div>
                                    <FontAwesomeIcon icon={faUserFriends} color={Colors.grey} size="lg"/>
                                    <span style={{marginLeft: 15, fontSize: 20}}><b>4746</b></span>
                                </div>
                                <div style={{marginTop: 10, fontSize: 20}}>
                                    rendez-vous enregistrés
                                </div>

                            </div>
                            <div  className="shadow  p-3 mb-5 bg-white rounded" style={{width: '30%'}}>
                                <div>
                                    <FontAwesomeIcon icon={faMale} color={Colors.grey} size="lg"/>
                                    <span style={{marginLeft: 15, fontSize: 20}}><b>182</b></span>
                                </div>
                                <div style={{marginTop: 10, fontSize: 20}}>
                                    Masculins
                                </div>
                            </div>
                            <div className="shadow p-3  mb-5 bg-white rounded" style={{width: '30%'}}>
                                <div>
                                    <FontAwesomeIcon icon={faFemale} color={Colors.grey} size="lg"/>
                                    <span style={{marginLeft: 15, fontSize: 20}}><b>200</b></span>
                                </div>
                                <div style={{marginLeft: 20, marginTop: 10, fontSize: 20}}>
                                    Feminins
                                </div>
                            </div>
                </div>
            </div>
            <Row className="justify-content-between">
                <Col xs={8}>
                    {renderGraph()}
                </Col>
                <Col xs={4}>
                    {renderList()}
                </Col>
            </Row>

        </main>
    );
};


export default CHomeScreen
