import React, {useState} from 'react';
import {useSelector} from "react-redux";
import {Button, Col, FormControl, Row, Spinner} from "react-bootstrap";

//hooks
import {useTranslation} from "react-i18next";
import useRematchDispatch from "../../hooks/useRematchDispatch";
import Page from "../../components/Page";
import PageTitle from "../../components/PageTitle";
import {Lock, UserPlus} from "react-feather";

const EmailFormat = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i

const UserCreateScreenOld = () => {
    const {t} = useTranslation();
    const {CreateUser} = useRematchDispatch(dispatch => ({
        CreateUser: dispatch.user.Create,
    }));

    const ongoing = useSelector(state => state.user.ongoing);
    const [errors, setErrors] = useState({
        firstName: false,
        lastName: false,
        userName: false,
        password: false,
    });
    const [inputs, setInputs] = useState({
        firstName: "",
        lastName: "",
        phoneNumber: "",
        userName: "",
        password: "",
    });

    const onPropertyValueChange = (event) => {
        setInputs({...inputs, [event.target.id]: event.target.value});
    };

    const handleClick = () => {
        if (inputs.firstName.length < 2) {
            setErrors(previous => ({...previous, "firstName": true}));
            return;
        } else {
            setErrors(previous => ({...previous, "firstName": false}));
        }
        if (inputs.lastName.length < 2) {
            setErrors(previous => ({...previous, "lastName": true}));
            return;
        } else {
            setErrors(previous => ({...previous, "lastName": false}));
        }
        if (!EmailFormat.test(inputs.userName)) {
            setErrors(previous => ({...previous, "userName": true}));
            return
        } else {
            setErrors(previous => ({...previous, "userName": false}));
        }
        if (inputs.password.length < 4) {
            setErrors(previous => ({...previous, "password": true}));
            return;
        } else {
            setErrors(previous => ({...previous, "password": false}));
        }
        CreateUser(inputs);
    };

    return (
        <Page>
            <div className="container">
                <Row>
                    <Col className="mb-3">
                        <PageTitle Icon={UserPlus} title={t(`labels.menu_create_user`)}/>
                    </Col>
                </Row>
                <div className="d-flex justify-content-center">
                        <div className="card-body">
                            <form>

                            </form>
                        </div>

                </div>
            </div>
        </Page>
    );
};

export default UserCreateScreenOld
