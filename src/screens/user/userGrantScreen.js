import React, { useEffect, useState } from 'react';
import { useSelector } from "react-redux";
import { ListGroup, ListGroupItem, Row, Col, Button, Spinner } from "react-bootstrap";

//hooks
import { useTranslation } from "react-i18next";
import useRematchDispatch from "../../hooks/useRematchDispatch";

//components
import UsersList from "../../components/usersList";
import OfficesList from "../../components/officesList";
import Page from "../../components/Page";
import PageTitle from "../../components/PageTitle";
import {Key} from "react-feather";
import {AppStyles} from "../../css/styles";


const privileges = [
    {code: "MA", name: "MANAGER"},
    {code: "OM", name: "OFFICE"},
    {code: "SM", name: "SERVICE"}
];
const UserGrantScreen = () => {
    const {t} = useTranslation();
    const {GetUsers, GrantUser, GetOffices} = useRematchDispatch(dispatch => ({
        GetUsers: dispatch.user.GetUsers,
        GrantUser: dispatch.user.GrantUser,
        GetOffices: dispatch.customer.GetOffices,
    }));

    const ongoing = useSelector(state => state.user.ongoing);
    const users = useSelector(state => state.user.users);
    const offices = useSelector(state => state.customer.offices);
    const customerId = useSelector(state => state.preference.customerId);

    const [input, setInput] = useState({});

    useEffect(() => {
        GetUsers();
        if (offices.length === 0 && customerId !== null) {
            GetOffices(customerId);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const userSelected = (userId) => {
        setInput(previous => ({...previous, "userId": userId}));
    };
    const officeServiceSelected = (officeId, serviceId) => {
        setInput(previous => ({...previous, "officeId": officeId, "serviceId": serviceId}));
    };

    const roleSelected = (code) => {
        setInput(previous => ({...previous, "roleCode": code}));
    };
    const handleClick = () => {
        const resourceId = localStorage.getItem('CustomerId');
        const pv = [{roleCode: input.roleCode, resourceId, officeId: input.officeId, serviceId: input.serviceId}];
        const data = {userId: input.userId, Privileges: pv};
        GrantUser(data);
    };
    const roles = () => {
        return (
            <main className="card">
                <div className="card-header" style={AppStyles.cardH}>
                    {t('roles')}
                </div>
                <div className="card-body">
                    <ListGroup>
                        {privileges.map((x) =>
                            <ListGroupItem action key={x.code} variant={`${x.code === input.roleCode ? "success" : ""}`}
                                           onClick={() => {
                                               roleSelected(x.code)
                                           }}>{x.name} </ListGroupItem>
                        )}
                    </ListGroup>
                </div>
            </main>
        )
    };

    return (
        <Page>
            <div className="container-fluid">
                <Col className="mb-3">
                    <PageTitle Icon={Key} title={t(`labels.menu_grant_user`)}/>
                </Col>
                <Row>
                    <Col xs="12" md={12} xl={3} className="mb-2"><UsersList users={users} onSelected={userSelected}/></Col>
                    <Col xs="12" md={12} xl={6} className="mb-2"><OfficesList offices={offices} onSelected={officeServiceSelected}/></Col>
                    <Col xs="12" md={12} xl={3}>{roles()}</Col>
                </Row>
                <span className="space"/>
                <Row>
                    <Col className="d-flex justify-content-center">
                        <Button variant="outline-dark" onClick={() => {
                            handleClick()
                        }} style={{width: '40%'}}>
                            {ongoing ?
                                <Spinner as="span" animation="grow" size="sm" role="status"
                                         aria-hidden="true"/> : null} {t('grant')}</Button>
                    </Col>
                </Row>
            </div>
        </Page>
    )
};

export default UserGrantScreen;
