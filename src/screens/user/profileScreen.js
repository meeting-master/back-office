import React from 'react';
//hooks
import Page from "../../components/Page";
import Activation from "./activation";
import ChangePasswordScreen from "./changePassword";
import Update from "./update";


const ProfileScreen = () => {

    return (
        <Page>
            <main className="container">
                <Activation/>
                <ChangePasswordScreen/>
                <Update />
            </main>
        </Page>
    );
};


export default ProfileScreen
