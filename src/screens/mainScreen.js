import React, {useEffect, useState} from 'react';
import {useSelector} from "react-redux";
import {BarChart, Clock, Settings, UserPlus} from "react-feather";
import {AccountCircle, Alarm, Business, CalendarToday, FlagSharp} from "@material-ui/icons";
import {makeStyles} from "@material-ui/core";

//hooks
import useRematchDispatch from "../hooks/useRematchDispatch";
import {useTranslation} from "react-i18next";

//screens
import AccountScreen from "./customer/accountScreen";
import HomeScreen from "./homeScreen";
import CreateScreen from "./user/createScreen";
import ScheduleScreen from "./calendar/scheduleScreen";
import OfficeScreen from "./customer/officeScreen";
import ServiceScreen from "./customer/serviceScreen";
import AppointmentScreen from "./calendar/appointmentScreen";
import CalendarScreen from "./calendar/calendarScreen";
import ProfileScreen from "./user/profileScreen";

//components
import NavBar from "./NavBar";
import TopBar from "../components/TopBar";
import {AlertMessage, Alerts} from "../components/alert";

//services
import Colors from "../utils/Colors";
import FONTS from "../utils/fonts";


const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: theme.palette.background.dark,
        display: 'flex',
        height: '100%',
        overflow: 'hidden',
        width: '100%'
    },
    wrapper: {
        display: 'flex',
        flex: '1 1 auto',
        overflow: 'hidden',
        paddingTop: 64,
        margin: 0,
        padding: 0,
        [theme.breakpoints.up('lg')]: {
            paddingLeft: 243
        },
        backgroundColor: '#fff',
    },
    contentContainer: {
        display: 'flex',
        flex: '1 1 auto',
        overflow: 'hidden',
        margin: 0,
        padding: 0,

    },
    content: {
        flex: 'auto',
        height: '100%',
        overflow: 'auto',

    },
}));

const Routes = {
    menu_dashboard: {screen: HomeScreen, icon: BarChart, title: "menu_dashboard"},
    menu_service: {screen: ServiceScreen, icon: FlagSharp, title: "menu_service"},
    menu_office: {screen: OfficeScreen, icon: Business, title: "menu_office"},
    menu_schedule: {screen: ScheduleScreen, icon: Clock, title: "menu_schedule"},
    menu_calendar: {screen: CalendarScreen, icon: CalendarToday, title: "menu_calendar"},
    menu_appointment: {screen: AppointmentScreen, icon: Alarm, title: "menu_appointment"},
    menu_settings: {screen: AccountScreen, icon: Settings, title: "menu_settings"},
    menu_create_user: {screen: CreateScreen, icon: UserPlus, title: "menu_manage_user"},
    menu_profile: {screen: ProfileScreen, icon: AccountCircle, title: "menu_profile"},
};


const MainScreen = () => {
    const {t} = useTranslation();

    const {GetAccount, LoadLocalData} = useRematchDispatch(dispatch => ({
        GetAccount: dispatch.customer.GetAccount,
        LoadLocalData: dispatch.preference.LoadLocalData,
    }));

    const error = useSelector(state => state.main.error);
    const success = useSelector(state => state.main.success);
    const currentCustomer = useSelector(state => state.customer.currentCustomer);
    const currentUser = useSelector(state => state.preference.user);
    const customerId = useSelector(state => state.preference.customerId);

    const [selected, setSelected] = useState('menu_dashboard');
    const [alert, setAlert] = useState({
        id: Date.now(),
        show: false,
        message: "",
        heading: "",
        type: ""
    });

    const classes = useStyles();
    const [isMobileNavOpen, setMobileNavOpen] = useState(false);
    /*
        useLayoutEffect(() => {
            const h = document.body.scrollHeight > window.innerHeight ? document.body.scrollHeight : window.innerHeight;
            setMenuHeight(h);
        });
    */

    useEffect(() => {
        if (customerId === null || currentUser === null) {
            LoadLocalData();
        }
        // eslint-disable-next-line
    }, []);

    useEffect(() => {
        if (customerId !== null) {
            GetAccount(customerId);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [customerId]);

    useEffect(() => {
        if (error !== null) {
            setAlert(a => AlertMessage(t(error.message), error.id, "danger"));
        }
        // eslint-disable-next-line
    }, [error]);

    useEffect(() => {
        if (success !== null) {
            setAlert(a => AlertMessage(t('success_message'), success, "success"));
            setTimeout(() => {
                setAlert({...alert, show: false})
            }, 5000)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [success]);

    const onSelect = (selected) => {
        if (selected === "menu_logout") {
            localStorage.clear();
            window.location.href = '/';
        } else {
            setSelected(selected);
            if (selected === "menu_dashboard") {
                GetAccount(customerId);
            }
        }
    };

    const route = Routes[selected] !== undefined ? Routes[selected] : Routes['menu_dashboard'];
    return (
        <div className={classes.root}>
            <header>
                <TopBar onMobileNavOpen={() => setMobileNavOpen(true)} onSelect={onSelect}
                        userName={currentUser?.firstName + ' ' + currentUser?.lastName}
                        customerName={currentCustomer?.name}
                        logoUrl={currentCustomer?.logoUrl} value={currentCustomer?.value}
                        rating={currentCustomer?.rating} icon={route.icon} title={route.title}/>

            </header>
            <nav>
                <NavBar
                    onMobileClose={() => setMobileNavOpen(false)}
                    openMobile={isMobileNavOpen}
                    onSelected={onSelect}
                    selectedKey={selected}
                />
            </nav>
            <main className={classes.wrapper}>
                <div className={classes.contentContainer}>
                    <article className={classes.content}>
                        <route.screen/>
                    </article>
                </div>
            </main>
            <footer style={styles.footer} className="linearGradient text-center">
              <span style={styles.link} className="name-link">
                <a href="https://easymeeting.io"
                   target="_blank" rel="noreferrer">Termes et conditions</a> | {'Copyright © '}Easy Meeting{' '}{new Date().getFullYear()}{'.'}
              </span>
            </footer>

            <Alerts key={alert.id} show={alert.show} type={alert.type} message={alert.message} heading={alert.heading}
                    onClose={() => {
                    }}/>
        </div>

    );
};

const styles = {
    link: {
        color: Colors.white,
        fontFamily: FONTS.Regular,
        fontSize: 11,
    },
    footer: {
        position: 'absolute',
        bottom: 0,
        height: 30,
        width: '100%'
    },

};

export default MainScreen
