import React, {useEffect, useState} from 'react';
import {Redirect, useHistory} from 'react-router-dom';
import {useSelector} from "react-redux";
import {Button, Col, Container, FormControl, InputGroup, Row, Spinner} from "react-bootstrap";
import '../css/login.css'
//hooks
import useRematchDispatch from "../hooks/useRematchDispatch";
import {useTranslation} from "react-i18next";
//services
import FONTS from "../utils/fonts";
import Colors from "../utils/Colors";
//components
import ForgetPasswordModal from "../components/forgetPasswordModal";
import {AlertMessage, Alerts} from "../components/alert";

import trame2 from "../images/trame2.png";
import {faEnvelope, faLock} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import Slider from "../components/Slider";
import Grid from "@material-ui/core/Grid";


const EmailFormat = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i

const LoginScreenNo = (props) => {
    const {t} = useTranslation();
    const history = useHistory();
    const {ForgotPassword, PostLogin, GetLogos} = useRematchDispatch(dispatch => ({
        PostLogin: dispatch.user.PostLogin,
        ForgotPassword: dispatch.user.ForgotPassword,
        GetLogos: dispatch.customer.GetLogos,
    }));
    const error = useSelector(state => state.user.error);
    const redirect = useSelector(state => state.user.redirect);
    const onLoading = useSelector(state => state.user.onLoading);
    const images = useSelector(state => state.customer.logoUrls);
    const [modalShow, setModalShow] = useState(false);
    const [alert, setAlert] = useState({
        show: false,
        message: "",
        heading: "",
        type: ""
    });
    const [errors, setErrors] = useState({
        email: false,
        password: false
    });

    const [inputs, setInputs] = useState({
        userName: "",
        password: ""
    });
    const {from} = props.location.state || {from: {pathname: "/"}};

    useEffect(() => {
        GetLogos()
    }, []);

    useEffect(() => {
        if (error !== null) {
            setAlert(alert => AlertMessage(t(error.message), Date.now(), "danger"))
        }

    }, [error]);

    useEffect(() => {
        setAlert(al => ({...al, show: false}));
    }, []);

    const handleClick = () => {
        if (!EmailFormat.test(inputs.userName)) {
            setErrors(inputs => ({...inputs, "email": true}));
            return
        } else {
            setErrors(inputs => ({...inputs, "email": false}));
        }
        if (inputs.password.length < 4) {
            setErrors(inputs => ({...inputs, "password": true}));
            return;
        } else {
            setErrors(inputs => ({...inputs, "password": false}));
        }
        PostLogin(inputs);
    };

    const onPropertyValueChange = (event) => {
        setInputs({...inputs, [event.target.id]: event.target.value});
    };

    const onForgotPasswordReturn = (value) => {
        setModalShow(false);
        ForgotPassword(value)
    };

    return (
        <>
            {redirect ? <Redirect to={from}/> :
                <Container fluid style={styles.root}>
                    <Row style={styles.middle}>
                        <Grid>

                        </Grid>
                        <Col className="" md={7} style={styles.image}>
                            <img src={trame2} alt={'Easy Meeting'} width={300}  />
                        </Col>
                        <Col md={5} xs={12} className="loginContainer" style={styles.container}>
                            <Row>
                                <Col xs={12} style={styles.formContainer}
                                     className="mb-5 mb-sm-2 mb-md-1 mb-xl-5 formContainer">
                                    <form style={styles.form}>
                                        <InputGroup className="mb-2 mb-sm-2 mb-md-1 mb-xl-5">
                                            <InputGroup.Prepend>
                                                <InputGroup.Text style={styles.prepend}>
                                                    <FontAwesomeIcon icon={faEnvelope} color={Colors.white}/>
                                                </InputGroup.Text>
                                            </InputGroup.Prepend>

                                            <FormControl type="text"
                                                         placeholder={t('email')}
                                                         id="userName"
                                                         isInvalid={errors.email}
                                                         onChange={(event) => onPropertyValueChange(event)}
                                                         style={styles.input}/>
                                        </InputGroup>

                                        <InputGroup className="mb-2 mb-sm-2 mb-md-0">
                                            <InputGroup.Prepend>
                                                <InputGroup.Text style={styles.prepend}>
                                                    <FontAwesomeIcon icon={faLock} color={Colors.white}/>
                                                </InputGroup.Text>
                                            </InputGroup.Prepend>
                                            <FormControl type="password"
                                                         placeholder={t('password')} id="password"
                                                         isInvalid={errors.password}
                                                         onChange={(event) => onPropertyValueChange(event)}
                                                         style={styles.input}/>
                                        </InputGroup>
                                        <div className="float-right">
                                        <span className="btn btn-link txt1" style={styles.passTitle}
                                              onClick={() => setModalShow(true)}>{t('forgot_password_text')}</span>
                                        </div>
                                        <div className="form-group d-flex justify-content-center mt-5">
                                            <Button onClick={() => {
                                                handleClick()
                                            }} className="w-100"
                                                    style={{backgroundColor: Colors.lightblue}}> {onLoading ?
                                                <Spinner as="span" animation="grow" size="sm" role="status"
                                                         aria-hidden="true"/> : null} {t('login')}</Button>
                                        </div>
                                    </form>
                                </Col>
                                <Col xs={12} style={{padding: 0}}>
                                    <Button className="login100-form-btn" onClick={() => history.push('/register')}
                                            style={{margin: 0, padding: 0}}>{t('register_your_account')}</Button>
                                </Col>
                            </Row>

                        </Col>
                    </Row>
                    <Row className="text-center align-items-center p-0 p-md-4 p-xl-3 xs-hidden">
                        <Col xs={12} sm={12} md={2} lg={3} className="text-center align-items-center">
                            <span style={styles.confidence}>{t('confidence')}</span>
                        </Col>
                        <Col xs={12} sm={12} md={8} lg={6} className="text-center align-items-center">
                            <Slider className="text-center align-items-center" images={images}/>
                        </Col>
                        <Col xs={12} sm={12} md={2} lg={3}/>

                    </Row>
                    <Row style={styles.footer} className="linearGradient text-center">
                        <Col>
                            <span style={styles.link} className="name-link">
                                <a href="https://easymeeting.io">Termes et conditions</a> | {'Copyright © '}Easy Meeting{' '}{new Date().getFullYear()}{'.'}
                            </span>
                        </Col>
                    </Row>

                    <ForgetPasswordModal show={modalShow} onHide={(value) => onForgotPasswordReturn(value)}/>
                    <Alerts show={alert.show} type={alert.type} message={alert.message} heading={alert.heading}
                            onClose={() => {
                            }}/>
                </Container>}
        </>
    );
};

const styles = {
    root: {
        display: 'flex',
        flexDirection: 'column',
        height: '100vh',
        backgroundColor: Colors.white,
    },

    header: {
        height: '20%',
    },

    middle: {
        backgroundColor: Colors.meanBlue,
        height: '80%',
    },

    image: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: '100%',
    },

    container: {
        flexDirection: 'column',
        marginTop: 0,
        backgroundColor: Colors.white,
        paddingHorizontal: 10,
    },

    formContainer: {
        backgroundColor: Colors.white,
        marginTop: 0,
        borderRadius: 5,
    },
    prepend: {
        backgroundColor: Colors.lightblue,
        color: Colors.white,
        borderColor: Colors.lightblue,
    },
    input: {
        fontFamily: FONTS.Italic,
        borderColor: Colors.lightblue,
        borderWidth: 0.1
    },

    logoContainer: {},

    avatar: {
        justifyContent: 'center',
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: 20,
        paddingHorizontal: 10,
    },
    submit: {
        backgroundColor: Colors.royalblue,
        color: Colors.white,
    },
    title: {
        marginTop: 30,
        color: Colors.greyhish,
        alignItems: 'center',
        fontFamily: FONTS.Regular,
        fontSize: 20,
    },
    passTitle: {
        color: '#636770',
        fontFamily: FONTS.Regular,
        fontSize: 11,
    },
    confidence:
        {
            color: Colors.meanBlue,
            fontFamily: FONTS.SemiBold,
            fontSize: 13,
        },
    link: {
        color: Colors.white,
        fontFamily: FONTS.Regular,
        fontSize: 11,
    },
    footer: {
        position: 'absolute',
        bottom: 0,
        height: 30,
        width: '100%'
    }
};

export default LoginScreenNo
