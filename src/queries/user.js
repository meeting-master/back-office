import axios from './axios'

const BASE = "/user";

export const Create = async (data) => {
    return axios.post(BASE+"/create", data)
};

export const Activation = async (data) => {
    return axios.post(BASE+"/activate", data)
};

export const ChangePassword = async (data) => {
    return axios.post(BASE+"/change-password", data)
};

export const UsersList = async () => {
    return axios.get(BASE + "/users")
};

export const UserGet = async () => {
    return axios.get(BASE)
};

export const UserUpdate = async (data) => {
    return axios.put(BASE, data)
};

export const Login = async (data) => {
    return axios.post(BASE+"/login", data)
};

export const Grant = async (data) => {
    console.log(data);
    return axios.post(BASE+"/grant", data)
};

export const ForgotPassword = async (userName) => {
    return axios.post(BASE+"/forgot-password", {userName})
};

export const GetPrivileges = async (userId) => {
    return axios.get(BASE + "/privileges", {
        params: {
            userId
        }})
};