import axios from './axios'

const BASE = "/calendar";

export const InsertSchedule = async (data) => {
    return axios.post(BASE + "/schedule", data)
};

export const UpdateSchedule = async (data) => {
    return axios.put(BASE + "/schedule", data)
};

export const GetSchedule = async (officeId, serviceId) => {
    return axios.get(BASE + "/schedule", {
        params: {
            officeId,
            serviceId
        }})
};

export const GetCalendarHistory = async (officeId, serviceId) => {
    return axios.get(BASE + "/calendar-history", {
        params: {
            officeId,
            serviceId
        }})
};

export const GetDayCalendar = async ({officeId, serviceId, day}) => {
    return axios.get(BASE + "/time-slot-by-dates", {
        params: {
            officeId,
            serviceId,
            dates: [day],
        }})
};

export const GenerateTimeSlots = async (data) => {
    return axios.post(BASE + "/generate-time-slots", data)
};

export const GetTimeSlotAppointments = async (timeSlotIds) => {
    return axios.get(BASE + "/time-slot-appointments", {
        params: {
            timeSlotIds
        }})
};

export const CreateAppointment = async (data) => {
    return axios.post(BASE + "/appointment", data)
};

export const CancelAppointment = async (data) => {
    return axios.delete(BASE + "/appointment", {
        params: {
            data
        }})
};

export const ConsumeAppointment = async (id) => {
    return axios.put(BASE + "/appointment", {id})
};

export const GetCalendar = async (data) => {
    return axios.post(BASE + "/calendar", data)
};

export const GetCalendarTimeSlots = async (officeId, serviceId, startDate, endDate) => {
    return axios.get(BASE + "/time-slot-by-date-range", {
        params: {
            officeId,
            serviceId,
            startDate,
            endDate
        }})
};
