import axios from './axios'

const BASE = "/location";

export function FetchCountries() {
    return axios.get(BASE + "/countries");
}

export function FetchLocations() {
    return axios.get(BASE + "/locations-by-district");
}
