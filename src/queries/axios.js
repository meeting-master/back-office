import axios from 'axios';
import Config from "../configurations";
axios.defaults.baseURL = Config.API_HOST;

axios.interceptors.request.use(
    config => {
        config.headers.authorization = `Bearer ${localStorage.getItem("Token")}`;
        return config
    },
    error => {
        return Promise.reject(error);
    }
);

export default axios;
