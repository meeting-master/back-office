import React from 'react';

import './App.css';
import './css/font.css'
import './css/common.css';
import './css/login.css';
import 'bootstrap/dist/css/bootstrap.css';

import { ThemeProvider } from '@material-ui/core';

import {ApplicationRoutes} from "./navigation/appRoutes";
import GlobalStyles from "./components/GlobalStyles";
import theme from './theme';
const App = () => {
    return (
        <ThemeProvider theme={theme}>
            <GlobalStyles />
            <ApplicationRoutes/>
        </ThemeProvider>

    );
};
export default App;
