import React from "react";
import {CartesianGrid, Legend, Line, LineChart, ResponsiveContainer, XAxis, YAxis} from "recharts";

const LinearGraph = ({data, style, legend}) => {
    return (
        <ResponsiveContainer width='90%' height={425}>
            <LineChart
                data={data}
                margin={{top: 15, right: 5, left: 20, bottom: 15}}
            >
                <CartesianGrid strokeDasharray="3 3"/>
                <XAxis dataKey="name"/>
                <YAxis/>
                <Legend/>
                {legend.map(l =>
                    <Line type="monotone" dataKey={l.key} stroke={l.color} activeDot={{r: 8}} key={l.key}/>
                )}

            </LineChart>
        </ResponsiveContainer>
    );
};
export default LinearGraph;
