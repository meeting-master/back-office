import React from "react";
import {Col, Row} from "react-bootstrap";
import {useTranslation} from "react-i18next";
import imageText from '../images/imageText.png'
//services
import Colors from "../utils/Colors";
import {IconButton} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import FONTS from "../utils/fonts";
import {LogOut} from "react-feather";

const Header = ({onMobileNavOpen, onSelect, userName}) => {
    const {t} = useTranslation();
    return (
        <Row style={styles.container} >
            <Col xs={9} md={10} xl={11} style={styles.bar}
                 className="d-flex align-items-center justify-content-between">
                <div className="d-flex  align-items-center">
                    <IconButton color="inherit" onClick={onMobileNavOpen}
                                className="d-block d-sm-block d-md-block d-xl-none">
                        <MenuIcon color="inherit"/>
                    </IconButton>
                    <img src={imageText} alt={'Easy Meeting'} className="ml-sm-5 mt-0 easy" width="20%"/>
                </div>
                <div style={styles.name}>
                    {userName}
                </div>

            </Col>
            <Col xs={3} md={2} xl={1} style={styles.logOutContainer}>
                <IconButton onClick={() => onSelect('menu_logout')} >
                    <LogOut style={{color: Colors.blueLight, marginLeft: -10}} size={15}/>
                    <span style={styles.logOut} className="ml-1"> {t('menu_logout')} </span>
                </IconButton>

            </Col>
        </Row>

    )
};

const styles = {
    container: {height: 50,},
    logOutContainer: {
        backgroundColor: Colors.meanBlue,
        margin: 0,
        padding: 0,
        fontSize: 11,
        color: Colors.white,
        fontFamily: FONTS.Regular,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    },
    bar: {background: `linear-gradient(190deg, ${Colors.lightblue}, ${Colors.royalblue})`},
    name: {
        color: Colors.white,
        fontFamily: FONTS.Regular,
    },
    logOut: {
        fontSize: 11,
        color: Colors.white,
        fontFamily: FONTS.Regular,
    }
};
export default Header;
