import React from "react";
import { Button, Navbar } from "react-bootstrap";
import { useTranslation } from "react-i18next";
import logotext from '../images/logotext.png'

//services
import FONTS from "../utils/fonts";
import Colors from "../utils/Colors";

const Header = (props) => {
    const {t} = useTranslation();
    return (
        <Navbar className="justify-content-between" style={{background:`linear-gradient(190deg, ${Colors.lightblue}, ${Colors.royalblue})`}}>
            <Navbar.Brand href="#" style={{fontFamily: FONTS.Italic}}> <img src={logotext} alt={'Easy Meeting'}/></Navbar.Brand>
            <Navbar.Text style={{fontFamily: FONTS.Regular,  color: Colors.white, fontSize:14}}>{props.pageName.toUpperCase()}</Navbar.Text>
            <div>
                <Button  style={{backgroundColor: Colors.meanBlue, color: Colors.white, fontSize: 11}}>{props.name} </Button><span style={{color: Colors.white}}>-</span>
                <Button  style={{backgroundColor: Colors.royalblue, color: Colors.white, fontSize: 11}}>{props.userName}</Button>
            </div>
        </Navbar>
    )
};
export default Header;
