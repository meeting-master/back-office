import React from "react";
import { Form, Table } from "react-bootstrap";
import { useTranslation } from "react-i18next";

const OfficesListTable = (props) => {
    const {t} = useTranslation();
    return (
        <Table striped bordered hover>
            <thead>
            <tr>
                <th>{t('name')}</th>
                <th>{t('alias')}</th>
                <th>{t('phone_number')}</th>
                <th>{t('archived')}</th>
            </tr>
            </thead>
            <tbody>
            {props.offices.map((x) =>
                <tr key={x.id} className={`${x.id === props.officeId ? "secondary" : ""}`} onClick={() => {
                    props.onSelected(x.id)
                }} style={{cursor: 'pointer'}}>
                    <td>{x.name}</td>
                    <td>{x.alias}</td>
                    <td>{x.phoneNumber}</td>
                    <td><Form.Check type="checkbox" checked={x.isArchived} disabled={true}  /></td>
                </tr>)}
            </tbody>
        </Table>
    )
}

export default OfficesListTable;
