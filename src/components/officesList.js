import React, { useEffect, useState } from "react";
import { Col, ListGroup, ListGroupItem, Row } from "react-bootstrap";
import { useTranslation } from "react-i18next";
import useRematchDispatch from "../hooks/useRematchDispatch";
import { useSelector } from "react-redux";
import {AppStyles} from "../css/styles";


const OfficesList = (props) => {
    const {t} = useTranslation();
    const {GetOfficeServices} = useRematchDispatch(dispatch => ({
        GetOfficeServices: dispatch.customer.GetOfficeServices,
    }));

    const officeServices = useSelector(state => state.customer.officeServices);

    const [officeId, setOfficeId] = useState("");
    const [serviceId, setServiceId] = useState("");

    useEffect(() => {
        setOfficeId(null);
        onServiceSelected(null);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.toReload]);

    const onChangeOffice = (id) => {
        setOfficeId(id);
        onServiceSelected(null);
        GetOfficeServices(id)
    };

    const offices = () => {
        return (
            <main className="card">
                <div className="card-header" style={AppStyles.cardH}>
                    {t('office_list')}
                </div>
                <div className="card-body">
                    <ListGroup>
                        {props.offices.filter(x => !x.isArchived).map((x) =>
                            <ListGroupItem action key={x.id} variant={`${x.id === officeId ? "secondary" : ""}`}
                                           onClick={() => {
                                               onChangeOffice(x.id)
                                           }}>{x.name} </ListGroupItem>
                        )}
                    </ListGroup>
                </div>
            </main>
        )
    };

    const services = () => {
        return (
            <main className="card">
                <div className="card-header" style={AppStyles.cardH}>
                    {t('services_list')}
                </div>
                <div className="card-body">
                    <ListGroup>
                        {officeServices.map(x =>
                            <ListGroupItem action key={x.id} variant={`${x.id === serviceId ? "info" : ""}`}
                                           onClick={() => {
                                               onServiceSelected(x.id)
                                           }}>{x.serviceName} </ListGroupItem>
                        )}
                    </ListGroup>
                </div>
            </main>
        )
    };
    const onServiceSelected = (id) => {
        setServiceId(id);
        props.onSelected(officeId, id);
    };
    return (
        <Row>
            <Col xs={12} md={12} xl={6} className="mb-2">{offices()}</Col>
            <Col xs={12} md={12} xl={6}>{services()}</Col>
        </Row>
    )
};

export default OfficesList;
