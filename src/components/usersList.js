import React, {useState} from "react";
import {ListGroup, ListGroupItem} from "react-bootstrap";
import {useTranslation} from "react-i18next";
import {AppStyles} from "../css/styles";

const UsersList = (props) => {
    const {t} = useTranslation();
    const [id, setId] = useState("");
    return (
        <main className="card">
            <div className="card-header" style={AppStyles.cardH}>
                {t('user_list')}
            </div>
            <div className="card-body">
                <ListGroup>
                    {props.users.map((user) =>
                        <ListGroupItem action key={user.ID} variant={`${user.ID === id ? "primary" : ""}`}
                                       onClick={() => {
                                           setId(user.ID);
                                           props.onSelected(user.ID)
                                       }}>{user.FirstName + " " + user.LastName} </ListGroupItem>
                    )}
                </ListGroup>
            </div>
        </main>
    )
};

export default UsersList;
