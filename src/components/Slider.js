import React from 'react';

import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';


const Slider = ({images}) => {
    const responsive = {
        desktop: {
            breakpoint: {max: 3000, min: 1024},
            items: images.length >= 8 ? 8 : images.length,
            slidesToSlide: images.length >= 4 ? 4 : images.length // optional, default to 1.
        },
        tablet: {
            breakpoint: {max: 1024, min: 464},
            items: 4,
            slidesToSlide: 2 // optional, default to 1.
        },
        mobile: {
            breakpoint: {max: 464, min: 0},
            items: 3,
            slidesToSlide: 2 // optional, default to 1.
        }
    };

    return (
        <Carousel
            customLeftArrow={<div/>}
            customRightArrow={<div/>}
            draggable={false}
            responsive={responsive}
            ssr={true} // means to render carousel on server-side.
            infinite={true}
            autoPlay={true}
            autoPlaySpeed={5000}
            keyBoardControl={true}
            customTransition="all .5"
            transitionDuration={500}
            containerClass="carousel-container"
            itemClass="carousel-item-padding-40-px">

            {images.map(x => {
                return <img
                    src={x.url}
                    alt={x.name}
                    width={50}
                    height={50}
                    key={x.name}
                />
            })}

        </Carousel>
    );
};

export default Slider;
