import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {AppBar, makeStyles} from '@material-ui/core';
import Colors from "../utils/Colors";
import Header from "./header";
import SubHeader from "./subheader";


const useStyles = makeStyles(() => ({
    root: {
        background: `linear-gradient(190deg, ${Colors.lightblue}, ${Colors.royalblue})`,
    },
    avatar: {
        width: 60,
        height: 60
    }
}));

const TopBar = ({
                    className,
                    onMobileNavOpen,
                    onSelect,
                    userName,
                    customerName,
                    logoUrl,
                    value,
                    rating,
                    icon,
                    title,
                    ...rest
                }) => {
    const classes = useStyles();
    //const [notifications] = useState([]);

    return (
        <AppBar
            className={clsx(classes.root, className)}
            elevation={0}
            {...rest}
        >
            <Header onMobileNavOpen={onMobileNavOpen} userName={userName} onSelect={onSelect}/>
            <SubHeader customerName={customerName} logoUrl={logoUrl} value={value} rating={rating} pageIcon={icon}
                       pageTitle={title}/>
        </AppBar>
    );
};

TopBar.propTypes = {
    className: PropTypes.string,
    onMobileNavOpen: PropTypes.func
};

export default TopBar;
