import React from "react";
import {Col, Row} from "react-bootstrap";
import Colors from "../utils/Colors";
import withStyles from "@material-ui/core/styles/withStyles";
import Rating from "@material-ui/lab/Rating";
import {makeStyles} from "@material-ui/core";
import {Activity} from "react-feather";
import FONTS from "../utils/fonts";
import PageTitle from "./PageTitle";


//services
const placeholder = "https://via.placeholder.com/250";

const useStyles = makeStyles((theme) => ({
    wrapper: {
        margin: 0,
        padding: 0,
        background: Colors.white,
        [theme.breakpoints.up('lg')]: {
            paddingLeft: 243
        }
    },

}));


const SubHeader = ({customerName, logoUrl, value, rating, pageIcon, pageTitle}) => {
    const classes = useStyles();
    const StyledRating = withStyles({
        iconFilled: {
            color: Colors.royalblue,
        },
        iconHover: {
            color: Colors.meanBlue,
        },
    })(Rating);

    return (
        <main className={classes.wrapper}>
            <Row style={styles.container}>
                <Col className="d-flex align-items-center justify-content-between">
                    <div className="img-right">
                        <img src={logoUrl?.length > 0 ? logoUrl : placeholder} alt={customerName}
                             className="ml-sm-5 mt-0 customerLogo" width="20%"/>
                        <div className="align-items-center mt-xs-0 mt-xl-1 pt-1">
                            <span style={styles.name}>{customerName}</span>
                            <div className="mt-xl-2 mt-xs-0">
                                <StyledRating defaultValue={rating} size="small" name={customerName} readOnly/>
                            </div>
                        </div>
                    </div>
                    <div className="pr-xs-1 pr-lg-5">
                        <Activity size={20} color={Colors.royalblue}/> <span style={styles.name}
                                                                             className="pl-1">: {value}</span>
                    </div>
                </Col>

            </Row>
            <Row>
                <Col className="mt-2 ml-4">
                    <PageTitle Icon={pageIcon} title={pageTitle}/>
                </Col>
            </Row>
        </main>

    )
};

const styles = {
    container: {
        height: 100,
        background: Colors.white,
    },
    name: {
        color: Colors.royalblue,
    },
    bar: {
        background: Colors.white,
        fontFamily: FONTS.Regular,
    }
};
export default SubHeader;
