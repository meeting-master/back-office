import React, { forwardRef } from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';

const Page = forwardRef(({
  children,
  title,
  ...rest
}, ref) => {
  return (
    <section
      ref={ref}
      {...rest}
      style={styles.root}
    >
      <Helmet>
        <title>{title}</title>
      </Helmet>
      {children}
    </section>
  );
});

Page.propTypes = {
  children: PropTypes.node.isRequired,
  title: PropTypes.string
};
const styles = {
  root: {
    minHeight: '100%',
    paddingBottom: 20,
    padding: 20,
    paddingTop: 120
  }
};

export default Page;
