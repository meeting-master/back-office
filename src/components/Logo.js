import React from 'react';
import logotext from '../images/logotext.png'
const Logo = (props) => {
  return (
    <img
      alt="Logo"
      src={logotext}
      {...props}
    />
  );
};

export default Logo;
