import React from 'react';
import PropTypes from 'prop-types';

import Colors from "../utils/Colors";
import FONTS from "../utils/fonts";
import {useTranslation} from "react-i18next";

const PageTitle = ({Icon, title}) => {
    const {t} = useTranslation();
    return (
        <div className="d-flex" style={styles.title}>
            <Icon style={styles.icon} size={25}/>
            <span className="ml-2">{t(title)}</span>
        </div>
    );
};

PageTitle.propTypes = {
    Icon: PropTypes.object.isRequired,
    title: PropTypes.string
};
const styles = {
    title: {
        marginLeft: 5,
        color: 'black',
        fontFamily: FONTS.Bold,
    },
    icon: {
        color: Colors.blueLight,
    },
};

export default PageTitle;
