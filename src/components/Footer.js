import React from 'react';
import {Col, Row} from "react-bootstrap";
import Colors from "../utils/Colors";
import FONTS from "../utils/fonts";
import Slider from "./Slider";
import {useTranslation} from "react-i18next";
import {useSelector} from "react-redux";

const Footer = () => {
  const {t} = useTranslation();
  const images = useSelector(state => state.customer.logoUrls);
    return (
        <>
            <Row className="text-center align-items-center p-0 p-md-4 p-xl-3 xs-hidden">
                <Col xs={12} sm={12} md={2} lg={3} className="text-center align-items-center">
                    <span style={styles.confidence}>{t('confidence')}</span>
                </Col>
                <Col xs={12} sm={12} md={8} lg={6} className="text-center align-items-center">
                    <Slider className="text-center align-items-center" images={images}/>
                </Col>
                <Col xs={12} sm={12} md={2} lg={3}/>

            </Row>
            <Row style={styles.footer} className="linearGradient text-center">
                <Col>
              <span style={styles.link} className="name-link">
                <a href="https://easymeeting.io">Termes et conditions</a> | {'Copyright © '}Easy Meeting{' '}{new Date().getFullYear()}{'.'}
              </span>
                </Col>
            </Row>
        </>
    );
};

const styles = {
    confidence: {
        color: Colors.meanBlue,
        fontFamily: FONTS.SemiBold,
        fontSize: 13,
    },
    link: {
        color: Colors.white,
        fontFamily: FONTS.Regular,
        fontSize: 11,
    },
    footer: {
        position: 'absolute',
        bottom: 0,
        height: 30,
        width: '100%'
    },

};
export default Footer;
