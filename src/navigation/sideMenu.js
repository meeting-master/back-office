import React from "react";
import SideNav, {NavItem, NavIcon, NavText} from '@trendmicro/react-sidenav';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {
    faPlaceOfWorship,
    faHome,
    faServer,
    faCog,
    faCalendar,
    faCalendarTimes,
    faUser, faOutdent, faUserFriends,
} from '@fortawesome/free-solid-svg-icons'
import '@trendmicro/react-sidenav/dist/react-sidenav.css';
import {useTranslation} from "react-i18next";
import FONTS from "../utils/fonts";
import Colors from "../utils/Colors";


const SideMenu = (props) => {
    const { t } = useTranslation();

    return (
        <SideNav onSelect={props.onSelect} onToggle={props.onToggle} expanded={props.expanded} className="menu-style" style={{height: props.height}}>
            <SideNav.Toggle/>
            <SideNav.Nav selected={props.selected}>
                <NavItem eventKey="menu_dashboard">
                    <NavIcon>
                        <FontAwesomeIcon icon={faHome} color={Colors.white}/>
                    </NavIcon>
                    <NavText style={{paddingRight: 32}} title= {t('menu_dashboard')}>
                        {t('menu_dashboard')}
                    </NavText>
                </NavItem>
                <NavItem eventKey="menu_calendar">
                    <NavIcon>
                        <FontAwesomeIcon icon={faCalendar} color={Colors.white}/>
                    </NavIcon>
                    <NavText style={{paddingRight: 32, fontFamily: FONTS.Bold}} title= {t('menu_calendar')}>
                        {t('menu_calendar')}
                    </NavText>
                    <NavItem eventKey="menu_calendar_view">
                        <NavText title={t('menu_calendar_view')}>
                            {t('menu_calendar_view')}
                        </NavText>
                    </NavItem>
                    <NavItem eventKey="menu_calendar_generate">
                        <NavText title={t('menu_calendar_generate')}>
                            {t('menu_calendar_generate')}
                        </NavText>
                    </NavItem>
                    <NavItem eventKey="menu_calendar_settings">
                        <NavText title={t('menu_calendar_settings')}>
                            {t('menu_calendar_settings')}
                        </NavText>
                    </NavItem>
                </NavItem>
                <NavItem eventKey="menu_appointment">
                    <NavIcon>
                        <FontAwesomeIcon icon={faCalendarTimes} color={Colors.white}/>
                    </NavIcon>
                    <NavText style={{paddingRight: 32}} title={t('menu_appointment')}>
                        {t('menu_appointment')}
                    </NavText>
                </NavItem>
                <NavItem eventKey="menu_office">
                    <NavIcon>
                        <FontAwesomeIcon icon={faPlaceOfWorship} color={Colors.white}/>
                    </NavIcon>
                    <NavText style={{paddingRight: 32}} title= {t('menu_office')}>
                        {t('menu_office')}
                    </NavText>
                </NavItem>
                <NavItem eventKey="menu_service">
                    <NavIcon>
                        <FontAwesomeIcon icon={faServer}/>
                    </NavIcon>
                    <NavText style={{paddingRight: 32}} title={t('menu_service')}>
                        {t('menu_service')}
                    </NavText>
                </NavItem>
                <NavItem eventKey="menu_settings">
                    <NavIcon>
                        <FontAwesomeIcon icon={faCog} color={Colors.white}/>
                    </NavIcon>
                    <NavText style={{paddingRight: 32}} title= {t('menu_settings')}>
                        {t('menu_settings')}
                    </NavText>
                </NavItem>
                <NavItem eventKey="menu_manage_user">
                    <NavIcon>
                        <FontAwesomeIcon icon={faUserFriends} color={Colors.white}/>
                    </NavIcon>
                    <NavText style={{paddingRight: 32, fontFamily: FONTS.Bold}} title= {t('menu_manage_user')}>
                        {t('menu_manage_user')}
                    </NavText>
                    <NavItem eventKey="menu_create_user">
                        <NavText title={t('menu_create_user')}>
                            {t('menu_create_user')}
                        </NavText>
                    </NavItem>
                    <NavItem eventKey="menu_grant_user">
                        <NavText title={t('menu_grant_user')}>
                            {t('menu_grant_user')}
                        </NavText>
                    </NavItem>
                </NavItem>
                <NavItem eventKey="menu_profile">
                    <NavIcon>
                        <FontAwesomeIcon icon={faUser} color={Colors.royalblue}/>
                    </NavIcon>
                    <NavText style={{paddingRight: 32}} title= {t('menu_profile')}>
                        {t('menu_profile')}
                    </NavText>
                    <NavItem eventKey="menu_profile_activation">
                        <NavText title={t('menu_profile_activation')}>
                            {t('menu_profile_activation')}
                        </NavText>
                    </NavItem>
                    <NavItem eventKey="menu_profile_change_password">
                        <NavText title={t('menu_profile_change_password')}>
                            {t('menu_profile_change_password')}
                        </NavText>
                    </NavItem>
                </NavItem>
                <NavItem eventKey="menu_logout">
                    <NavIcon>
                        <FontAwesomeIcon icon={faOutdent} color={Colors.white}/>
                    </NavIcon>
                    <NavText style={{paddingRight: 32}} title= {t('menu_logout')}>
                        {t('menu_logout')}
                    </NavText>
                </NavItem>
            </SideNav.Nav>
        </SideNav>
    )
}


export default SideMenu;
