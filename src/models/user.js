import {createModel} from "@rematch/core";
import * as Request from "../queries/user";
import {Login as LoginRequest} from "../queries/user";
import {GenerateError} from "../utils/errors";

export const UserModel = () =>
    createModel({
        state: {
            ongoing: false,
            redirect: false,
            error: null,
            user: {},
            users: [],
            userPrivileges: [],
        },
        reducers: {
            errors(state, err) {
                return {...state, error: err, ongoing: false}
            },
            inProgress(state, ok, r) {
                return {...state, ongoing: ok, redirect: r}
            },
            updateUsers(state, users) {
                return {...state, users: users}
            },
            updateUser(state, user) {
                return {...state, user: user}
            },
            updateUserPV(state, pv) {
                return {...state, userPrivileges: pv}
            },
        },

        effects: dispatch => ({
            async Create(data) {
                this.inProgress(true);
                try {
                    await Request.Create(data);
                    this.inProgress(false);
                    dispatch.main.SetSuccess();
                } catch (err) {
                    dispatch.main.SetError(err)
                }
            },

            async Update(data) {
                this.inProgress(true);
                try {
                    await Request.UserUpdate(data);
                    this.inProgress(false);
                    dispatch.main.SetSuccess();
                    this.Get();
                } catch (err) {
                    dispatch.main.SetError(err)
                }
            },

            async PostLogin(payload) {
                this.inProgress(true);
                try {
                    const result = await LoginRequest(payload);
                    dispatch.preference.SetPreferences(result.data.response);
                    setLocalStorage(result.data.response);
                    this.inProgress(false, true)
                } catch (err) {
                    this.errors(GenerateError(err));
                }

            },

            async Activate(data, payload) {
                this.inProgress(true);
                try {
                    await Request.Activation(data);
                    this.inProgress(false);
                    dispatch.main.SetSuccess();
                } catch (err) {
                    this.inProgress(false);
                    dispatch.main.SetError(err)
                }
            },

            async ChangePassword(data, payload) {
                this.inProgress(true);
                try {
                    await Request.ChangePassword(data);
                    this.inProgress(false);
                    dispatch.main.SetSuccess();
                } catch (err) {
                    this.inProgress(false);
                    dispatch.main.SetError(err)
                }
            },
            async GetUsers() {
                try {
                    const result = await Request.UsersList();
                    this.updateUsers(result.data.response);
                } catch (err) {
                    dispatch.main.SetError(err)
                }
            },
            async Get() {
                try {
                    const result = await Request.UserGet();
                    this.updateUser(result.data.response);
                } catch (err) {
                    dispatch.main.SetError(err)
                }
            },

            async GetPrivileges(userId) {
                try {
                    const result = await Request.GetPrivileges(userId);
                    this.updateUserPV(result.data.response);
                } catch (err) {
                    dispatch.main.SetError(err)
                }
            },

            async GrantUser(data, payload) {
                this.inProgress(true);
                try {
                    await Request.Grant(data);
                    this.inProgress(false);
                    dispatch.main.SetSuccess();
                } catch (err) {
                    this.inProgress(false);
                    dispatch.main.SetError(err)
                }
            },
            async ForgotPassword(email) {

                try {
                    await Request.ForgotPassword(email);
                    dispatch.main.SetSuccess();
                } catch (err) {
                    this.errors(GenerateError(err));
                }
            },
        })
    });

const setLocalStorage = (data) => {
    const user = JSON.stringify({firstName: data.FirstName, lastName: data.LastName})
    localStorage.setItem("user", user);
    localStorage.setItem("Token", data.Token);
    localStorage.setItem("CustomerId", data.CustomerId);
};
