import {createModel} from "@rematch/core";
import * as Request from "../queries/customer";
import Colors from "../utils/Colors";
import {format} from 'date-fns';

export const StatisticModel = () =>
    createModel({
        state: {
            graphData: null,
            officeSummaries: [],
            loadedData: [],
            statData: {},
            appointmentCountByOfficeId: {},
            canceledCountByOfficeId: {},
            consumedCountByOfficeId: {},
        },
        reducers: {
            updateDataStreams(state, graphData) {
                return {...state, graphData: graphData}
            },
            updateStat(state, statData) {
                return {...state, statData: statData}
            },
            updateLoad(state, data) {
                return {...state, loadedData: data}
            },
            updateAppointmentCountByOfficeId(state, appointmentCountByOfficeId) {
                return {...state, appointmentCountByOfficeId: appointmentCountByOfficeId}
            },
            updateCanceledCountByOfficeId(state, canceledCountByOfficeId) {
                return {...state, canceledCountByOfficeId: canceledCountByOfficeId}
            },
            updateConsumedCountByOfficeId(state, consumedCountByOfficeId) {
                return {...state, consumedCountByOfficeId: consumedCountByOfficeId}
            },
        },

        effects: dispatch => ({

            async LoadStatistics(payload) {
                this.updateAppointmentCountByOfficeId({});
                this.updateCanceledCountByOfficeId({});
                this.updateConsumedCountByOfficeId({});
                try {
                    const result = await Request.GetOfficeStatistics(payload.customerId, payload.startDate, payload.endDate);
                    this.updateLoad(result.data.response);
                    if (result.data.response !== null) {
                        const {appointment, canceled, consumed} = result.data.response;
                        this.updateAppointmentCountByOfficeId(formatCountByOfficeId(appointment));
                        this.updateCanceledCountByOfficeId(formatCountByOfficeId(canceled));
                        this.updateConsumedCountByOfficeId(formatCountByOfficeId(consumed));
                    }
                } catch (err) {
                    dispatch.main.SetError(err);
                }

            },

            async LoadOfficeStatistics(payload, state) {
                try {
                    const officeData = state.statistic.loadedData?.filter(x => x.id === payload.id);
                    if (!officeData) {
                        return;
                    }
                    const {stats, graph} = formatData(officeData, payload.format);
                    this.updateStat(stats);
                    this.updateDataStreams(graph);
                } catch (err) {
                    dispatch.main.SetError(err);
                }

            },

        })
    });

const graphColor = [Colors.primary, Colors.greyhish, Colors.blueOne];

const formatCountByOfficeId = (loadedData) => {
    let countByOfficeId = {};
    loadedData.forEach(data => {
        countByOfficeId[data.officeId] = data.value;
    });
    return countByOfficeId;
};

const formatData = (loadedData, formatString) => {
    let count = 0, canceled = 0, consumed = 0;
    let dataMap = new Map();
    let legend = []; // legend to use by the graph component
    let i = 0;
    loadedData.forEach(office => {
        count += office.statistics.count ? office.statistics?.count : 0;
        canceled += office.statistics.canceled ? office.statistics?.canceled : 0;
        consumed += office.statistics.consumed ? office.statistics?.consumed : 0;
        //graph

        office.services.forEach(service => {
            //set legend as service name and set it color
            legend.push({key: service.name, color: graphColor[i % 2]});
            service.AppointmentStreams.sort((a, b) => {
                return Date.parse(a.date) - Date.parse(b.date);
            }).forEach(dataStream => {
                const key = format(Date.parse(dataStream.date), formatString);
                let ds = dataMap.get(key); // get the current date values in the map
                if (ds !== undefined) {
                    ds[service.name] = dataStream.count // add a new value if the current value exist
                } else { // other wise create the entry
                    ds = {name: key};
                    ds[service.name] = dataStream.count;
                }
                dataMap.set(key, ds); // put the new value into the map
            });
            i++;
        });
    });
    let values = [];
    for (const v of dataMap.values()) {
        values.push(v);
    }
    //return {dataStreams: values, legend};
    return {stats: {count, canceled, consumed}, graph: {dataStreams: values, legend}};
};
//here we format data for the graph
// const formatDataStream = (data) => {
//     let dataMap = new Map();
//     let legend = []; // legend to use by the graph component
//     let i = 0;
//     data.forEach(service => {
//         //set legend as service name and set it color
//         legend.push({key: service.name, color: i % 2 === 0 ? 'red' : 'green'});
//         service.dataStreams.forEach(dataStream => {
//             let ds = dataMap.get(dataStream.date); // get the current date values in the map
//             if (ds !== undefined) {
//                 ds[service.name] = dataStream.value // add a new value if the current value exist
//             } else { // other wise create the entry
//                 ds = {name: dataStream.date};
//                 ds[service.name] = dataStream.value;
//             }
//             dataMap.set(dataStream.date, ds); // put the new value into the map
//         });
//         i++;
//     });
//     //after having the data map here we transform it by getting his values as array
//     // the graph component may have array as param
//     let values = [];
//     for (const v of dataMap.values()) {
//         values.push(v);
//     }
//     return {dataStreams: values, legend};
// };
