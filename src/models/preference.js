import { createModel } from "@rematch/core";

export const PreferenceModel = () =>
    createModel({
        state: {
            user: {},
            customerId: null,
        },
        reducers: {
            updateState(state, id, user) {
                return {...state, user: user, customerId: id}
            },
            updateUsers(state, users) {
                return {...state, users: users}
            },
        },

        effects: dispatch => ({
            async LoadLocalData() {
                const customerId = localStorage.getItem("customerId");
                const user = localStorage.getItem("user")
                this.updateState(customerId, JSON.parse(user));
            },
             SetPreferences(data) {
                 const user = {firstName: data.FirstName, lastName: data.LastName}
                 this.updateState(data.CustomerId, user)
                 localStorage.setItem("Token", data.Token);
                 localStorage.setItem("customerId", data.CustomerId);
                 localStorage.setItem("user", JSON.stringify(user));
            },
        })
    });
