import {createModel} from "@rematch/core";
import { GenerateError } from "../utils/errors";

export const MainModel = () =>
    createModel({
        state: {
            error: null,
            success: null,
        },
        reducers: {
            errors(state, err) {
                return {...state, error: err}
            },
            success(state, id) {
                return {...state, success: id}
            },

        },
        effects: dispatch => ({
            SetError(err) {
                this.errors(GenerateError(err))
            },
            SetSuccess() {
                this.success(Date.now());
            },
            SetRuntimeError(err) {
                this.errors({id: Date.now()+1,
                    message: 'errors.'+err})
            },
        })
    });

