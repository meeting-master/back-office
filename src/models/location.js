import { createModel } from "@rematch/core";
import *as Request from "../queries/location";

export const LocationModel = () =>
    createModel(
        {
            state: {
                countries: [],
                towns: [],
                districts: [],
                locations:[]
            },
            reducers: {
                updateCountries(state, countries) {
                    return {...state, countries: countries}
                },
                updateLocations(state, locs) {
                    return {...state, locations: locs}
                },
            },

            effects: dispatch => ({
                async LoadCountries(payload, rootState) {
                    await Request.FetchCountries()
                        .then(result => dispatch.updateCountries(result))
                },
                async LoadLocations(payload, rootState) {
                    try {
                        const result = await Request.FetchLocations()
                        this.updateLocations(result.data.response);
                    } catch (err) {
                        dispatch.main.SetError(err)
                    }
                }
            })
        });
