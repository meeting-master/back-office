import { createModel } from "@rematch/core";
import * as Request from "../queries/calendar";
import {getDay} from 'date-fns'

export const CalendarModel = () =>
    createModel({
        state: {
            ongoing: false,
            error: null,
            serviceCalendar: [],
            schedules: [],
            timeSlotAppointments: [],
            calendarTimeSlotByHours: {},
            calendarHistory: [],
            dayCalendar: [],
            timeSlotByIds: {},
            reload: false,
        },
        reducers: {
            errors(state, err) {
                return {...state, error: err}
            },
            updateOngoing(state, ok) {
                return {...state, ongoing: ok}
            },
            updateReload(state, ok) {
                return {...state, reload: ok}
            },
            updateCalendar(state, sc) {
                return {...state, ongoing: false, serviceCalendar: sc}
            },
            updateSchedule(state, sch) {
                return {...state, schedules: sch}
            },
            updateTimeSlotAppointment(state, tsa) {
                return {...state, timeSlotAppointments: tsa}
            },
            updateCalendarTimeSlotByHour(state, cts) {
                return {...state, calendarTimeSlotByHours: cts}
            },
            updateHistory(state, h) {
                return {...state, calendarHistory: h}
            },
            updateDayCalendar(state, c) {
                return {...state, dayCalendar: c}
            },
            updateTimeSlotByIds(state, tsByIds) {
                return {...state, timeSlotByIds: tsByIds}
            }
        },

        effects: dispatch => ({
            async SetSchedule(payload) {
                this.updateOngoing(true);
                try {
                    await Request.InsertSchedule(payload);
                    this.updateOngoing(false);
                    dispatch.main.SetSuccess();
                } catch (err) {
                    this.updateOngoing(false);
                    dispatch.main.SetError(err);
                }
            },

            async GetSchedule({officeId, serviceId}) {
                if (officeId === "" || serviceId === "") return;
                try {
                    const result = await Request.GetSchedule(officeId, serviceId);
                    this.updateSchedule(result.data.response !== null ? result.data.response : []);
                } catch (err) {
                    dispatch.main.SetError(err);
                }
            },

            async UpdateSchedule(payload) {
                this.updateOngoing(true);
                try {
                    const result = await Request.UpdateSchedule(payload);
                    this.updateOngoing(false);
                    dispatch.main.SetSuccess();
                    this.updateSchedule(result.data.response !== null ? result.data.response : []);
                } catch (err) {
                    this.updateOngoing(false);
                    dispatch.main.SetError(err);
                }
            },

            async GenerateTimeSlots(payload) {
                this.updateOngoing(true);
                try {
                    await Request.GenerateTimeSlots(payload);
                    this.updateOngoing(false);
                    dispatch.main.SetSuccess();
                } catch (err) {
                    this.updateOngoing(false);
                    dispatch.main.SetError(err);
                }
            },

            async GetCalendarHistory({officeId, serviceId}) {
                try {
                    const result = await Request.GetCalendarHistory(officeId, serviceId);
                    this.updateHistory(result.data.response !== null ? result.data.response : []);
                } catch (err) {
                    dispatch.main.SetError(err);
                }
            },

            async GetDayCalendar(payload) {
                try {
                    const result = await Request.GetDayCalendar(payload);
                    this.updateDayCalendar(result.data.response !== null ? result.data.response : []);
                    this.updateReload(false);
                } catch (err) {
                    dispatch.main.SetError(err);
                }
            },

            async CreateAppointment(payload) {
                this.updateOngoing(true);
                try {
                    await Request.CreateAppointment(payload);
                    this.updateOngoing(false);
                    this.updateCalendar([]);
                    this.updateReload(true);
                    dispatch.main.SetSuccess();
                } catch (err) {
                    this.updateOngoing(false);
                    dispatch.main.SetError(err);
                }
            },

            async CancelAppointment(payload) {
                this.updateOngoing(true);
                try {
                    await Request.CancelAppointment(payload);
                    this.updateOngoing(false);
                    this.updateTimeSlotAppointment([]);
                    this.updateReload(true);
                    dispatch.main.SetSuccess();
                } catch (err) {
                    this.updateOngoing(false);
                    dispatch.main.SetError(err);
                }
            },

            async ConsumeAppointment(id) {
                this.updateOngoing(true);
                try {
                    await Request.ConsumeAppointment(id);
                    this.updateOngoing(false);
                    this.updateTimeSlotAppointment([]);
                    dispatch.main.SetSuccess();
                } catch (err) {
                    this.updateOngoing(false);
                    dispatch.main.SetError(err);
                }
            },



            async GetCalendar(payload) {
                try {
                    const result = await Request.GetCalendar(payload);
                    this.updateCalendar(result.data.response !== null ? result.data.response : []);
                    this.updateTimeSlotAppointment([]);
                } catch (err) {
                    dispatch.main.SetError(err);
                }
            },

            async GetTimeSlotAppointments(tsIds) {
                if (tsIds === null) return;
                this.updateOngoing(true);
                this.updateTimeSlotAppointment([]);
                try {
                    const result = await Request.GetTimeSlotAppointments(tsIds);
                    this.updateTimeSlotAppointment(result.data.response !== null ? result.data.response : []);
                    this.updateOngoing(false);
                } catch (err) {
                    dispatch.main.SetError(err);
                    this.updateOngoing(false);
                }
            },

            async GetCalendarTimeSlots(payload) {
                this.updateCalendarTimeSlotByHour({});
                this.updateTimeSlotAppointment([]);
                this.updateTimeSlotByIds({});
                try {
                    const result = await Request.GetCalendarTimeSlots(payload.officeId, payload.serviceId, payload.startDate, payload.endDate);
                    let timeSlotByStartTimes = [];
                    let timeSlotByHours = {};
                    let timeSlotByIds = {};
                    if (result.data.response !== null) {
                        result.data.response.forEach(element => {
                            let timeSlotByStartTime = {
                                startTime: element.startTime,
                                0: [],
                                1: [],
                                2: [],
                                3: [],
                                4: [],
                                5: [],
                                6:[]
                            };
                            if (element.consumed > 0) {
                                let timeSlotWeekDay = getDay(new Date(element.startDateTime));
                                timeSlotByStartTime[timeSlotWeekDay] = [element.id];
                            }
                            timeSlotByStartTimes.push(timeSlotByStartTime);
                        });

                        timeSlotByHours = timeSlotByStartTimes.reduce(
                            function (acc, obj) {
                                let key = parseInt(obj.startTime.substr(0,2), 10);
                                if (!acc[key]) {
                                    let {startTime, ...newObj} = obj;
                                    acc[key] = [];
                                    acc[key].push(newObj);
                                } else {
                                    let accObj = acc[key][0];
                                    for (let property in accObj) {
                                        accObj[property] = accObj[property].concat(obj[property]);
                                    }
                                    acc[key].push(accObj);
                                }
                                return acc;
                            },
                        {});
                        this.updateCalendarTimeSlotByHour(timeSlotByHours);
                        
                        timeSlotByIds = result.data.response.reduce(
                            function (acc, obj) {
                                let key = obj.id;
                                if (!acc[key]) {
                                    acc[key] = [];
                                }
                                acc[key].push(obj);
                                return acc;
                            },
                        {});
                        this.updateTimeSlotByIds(timeSlotByIds);
                    }
                } catch (err) {
                    dispatch.main.SetError(err);
                }
            },
        })
    });
